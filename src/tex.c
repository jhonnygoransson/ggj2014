#include "SOIL.h"
#include "pxfaux.h"

#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"

int tex_load( lua_State *L )
{
	int            width, height, channels;
	unsigned char* datan = 0;
	const char*    path = luaL_checkstring(L, 1);

	int id = SOIL_load_OGL_texture(path,
		SOIL_LOAD_RGBA, SOIL_CREATE_NEW_ID,
		SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT);
	
	if (id == 0)
	{
		lua_pushnil( L );
		lua_pushfstring(L, "Failed to load texture: %s", path);
		return 2;
	}
	

	datan = SOIL_load_image(path,&width,&height, &channels,SOIL_LOAD_RGBA);
	SOIL_free_image_data(datan);
	
	lua_pushinteger(L, id);
	lua_pushinteger(L, width);
	lua_pushinteger(L, height);
	return 3;
}