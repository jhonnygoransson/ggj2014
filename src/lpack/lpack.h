#ifndef _LPACK_H_
#define _LPACK_H_

int l_unpack(lua_State *L);
int l_pack(lua_State *L);

#endif /* _LPACK_H_  */