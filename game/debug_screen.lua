local gl  = require( "ffi/OpenGL" )
local pxf = require('pxf')

local camera = require("game/camera")

local debug_screen = {}

shader  = pxf.shader("data/shaders/tile.vs", "data/shaders/tile.fs", true)


function debug_screen:init(game)
	self.map = require("data/maps/grassy")
	self.time = 0
	camera:set_gameobject(game)

	self.scale = 1
	camera:zoom(self.scale)

	-- Calculate uv coordinates for tile index in tileset
	function get_uv(tile_idx, tileset)
		-- Tile index starts at 1, so offset now to avoid fiddling with 1's
		tile_idx = tile_idx - 1

		local n_tiles_w = math.floor(
			tileset.imagewidth / (tileset.tilewidth + tileset.spacing))
		local n_tiles_h = math.floor(
			tileset.imageheight / (tileset.tileheight + tileset.spacing))

		assert(n_tiles_w * n_tiles_h > tile_idx and tile_idx >= 0,
			   "tile_idx out of bounds: " .. tile_idx)

		local imagewidth = n_tiles_w * (tileset.tilewidth + tileset.spacing)
		local imageheight = n_tiles_h * (tileset.tileheight + tileset.spacing)

		local idx_x = tile_idx % n_tiles_w
		local idx_y = math.floor(tile_idx / n_tiles_w)

		assert(idx_x < n_tiles_w, "tile x offset can't be "..idx_x.."!")
		assert(idx_y < n_tiles_h, "tile y offset can't be "..idx_y.."!")

		local left_px = (idx_x * (tileset.tilewidth + tileset.spacing))
		local top_px = (idx_y * (tileset.tileheight + tileset.spacing))

		return {
			{
				left_px / tileset.imagewidth,  -- s0
				top_px / tileset.imageheight -- t0
			},

			{
				(left_px + tileset.tilewidth) / tileset.imagewidth, -- s1
				top_px / tileset.imageheight, -- t1
			},

			{
				(left_px + tileset.tilewidth) / tileset.imagewidth, -- s2
				(top_px + tileset.tileheight) / tileset.imageheight, -- t2
			},

			{
				left_px / tileset.imagewidth, -- s3
				(top_px + tileset.tileheight) / tileset.imageheight -- t3
			}
		}
	end

	-- some tests :S
	local tileset = self.map.tilesets[1]
	assert(get_uv(3, tileset)[1][1] == ((70+2)*2)/tileset.imagewidth)
	assert(get_uv(3, tileset)[2][1] == (((70+2)*2)+70)/tileset.imagewidth)
	assert(get_uv(15, tileset)[1][2] == ((70+2)*1)/tileset.imageheight)

	-- Add a pxf.quadbatch to a map layer, using tile set
	function build_layer(map_layer, tile_set)
		local q = pxf.quadbatch()
		q:begin()

		for i=1,#map_layer.data do
			tile_idx = map_layer.data[i]
			if tile_idx == 0 then
				goto next
			end
			q:setcoords(get_uv(tile_idx, tile_set))

			local x = (i - 1) % map_layer.width -- +1, since index starts at 1
			local y = math.floor((i-1) / map_layer.width) -- dito

			physics.new_platform(x, y, 1, 1)
			q:addtopleft(x, y, 1, 1)
::next::
		end

		q:finish()

		map_layer.quadbatch = q
	end

	-- physics
	physics = require("game/physics")

	for i=1,#self.map.layers do
		build_layer(self.map.layers[i], self.map.tilesets[1])
	end

	pl1 = physics.new_player()
	pl1:setPosition(8, 29)

	-- camera:lock_to( pl1 )
end


function debug_screen:update(game, dt, vis)
	self.time = self.time + dt
	physics.update(dt)

	repeat
		local ev = game:popevent()
		if ev ~= nil then
			if ev.action == "OFF" then
				if ev.key == "ESC" then
					game.running = false
				elseif ev.key == "LEFT" or ev.key == "PL1_MIC1" then
					physics.players[1]:goLeft(false)

					camera:translate(1,0)

				elseif ev.key == "RIGHT" or ev.key == "PL1_MIC4" then
					physics.players[1]:goRight(false)

					camera:translate(-1,0)
				elseif ev.key == "UP" then
					physics.players[1]:goRight(false)

					camera:translate(0,1)
				elseif ev.key == "DOWN" then
					physics.players[1]:goRight(false)

					camera:translate(0,-1)
				end

			elseif ev.action == "ON" then
				if ev.key == "LEFT" or ev.key == "PL1_MIC1" then
					physics.players[1]:goLeft(true)
				elseif ev.key == "RIGHT" or ev.key == "PL1_MIC4" then
					physics.players[1]:goRight(true)
				elseif ev.key == "UP" or ev.key == "PL1_MIC2"  then
					physics.players[1]:jump()
					--self.scale = self.scale * 1.1
					--camera:zoom(self.scale)
				elseif ev.key == "DOWN" or ev.key == "PL1_MIC3"  then
					self.scale = self.scale * 0.9
					--camera:zoom(self.scale)
				end
			end
		end
	until ev == nil

	-- :D
	-- camera:zoom(1+0.2*math.sin(self.time*4))
	camera:update(dt)

	--local x,y = physics.players[1]:getPosition()
	-- print("POS", camera.x,camera.y)
end

function debug_screen:draw(game, vis)
	if (vis < 1) then
		return
	end

	gl.glDisable( gl.GL_DEPTH_TEST )
	gl.glViewport(0, 0, game.settings.win_w, game.settings.win_h)
	gl.glClearColor(0.8125, 0.953125, 0.96484375, 1.0)
	gl.glClear( gl.GL_COLOR_BUFFER_BIT )

	gl.glEnable( gl.GL_BLEND )
	gl.glBlendFunc( gl.GL_SRC_ALPHA, gl.GL_ONE_MINUS_SRC_ALPHA )
	gl.glBlendEquation( gl.GL_FUNC_ADD )


	local tile_width = self.map.tilesets[1].tilewidth
	local tile_height = self.map.tilesets[1].tileheight

	shader:bind()
	assets["tile_texture"]:bind(0)
	shader:setuniform("mat4", "p_matrix", camera:get_projection())

	for i=1,#self.map.layers do
		self.map.layers[i].quadbatch:draw(shader,
			{position = true, uv0 = true, normal = false})
	end

	shader:unbind()
end


return debug_screen
