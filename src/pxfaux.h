#ifndef _PXFAUX_H_
#define _PXFAUX_H_

#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"

#ifdef WIN32
    #define LUA_MODULE __declspec(dllexport)
#else
    #define LUA_MODULE
#endif

int tex_load( lua_State *L );
int LUA_MODULE luaopen_pxfaux(lua_State* L);

#endif /* _PXFAUX_H_ */