local pt = {}

function pt.update(self, game, dt, vis)
	physics.update(5/60)

	repeat
		local ev = game:popevent()
		if ev ~= nil then
			if ev.action == "OFF" then
				if ev.key == "ESC" then
					game.running = false
				elseif ev.key == "LEFT" then
					physics.players[1]:goLeft(false)
				elseif ev.key == "RIGHT" then
					physics.players[1]:goRight(false)
				end
			elseif ev.action == "ON" then
				if ev.key == "LEFT" then
					physics.players[1]:goLeft(true)
				elseif ev.key == "RIGHT" then
					physics.players[1]:goRight(true)
				elseif ev.key == "UP" then
					physics.players[1]:jump()
				end
			end
		end
	until ev == nil
end

function pt.init(self, game)
	physics = require("game/physics")
	physics.init(arg[1], arg[2])

	-- players
	pl1 = physics.new_player(1)
	pl1:setPosition(100, 20)
	l2 = physics.new_player(2)
	l2:setPosition(50, 50)

	-- platform
	physics.new_platform(300, 200, 600, 5)

	shader = pxf.shader("data/shaders/physicstest.vs", "data/shaders/physicstest.fs", true)
	qb = pxf.quadbatch()
end

function pt.draw(self, game, vis)
	qb:begin()

	for i, p in pairs(physics.players) do
		local x, y = p:getPosition()
		local width, height = p:getSize()
		qb:addcentered(x, y, width, height)
	end

	for i, p in ipairs(physics.platforms) do
		qb:addcentered(p.x, p.y, p.width, p.height)
	end

	qb:finish()

	shader:bind()
	shader:setuniform("mat4", "pmtx", mat4.ortho(0, game.settings.win_w, game.settings.win_h, 0, 0, 1))
	qb:draw(shader)
	shader:unbind()

	for i, p in pairs(physics.players) do
		local x, y = p:getPosition()
		gamefont.draw(x,y,16,"p"..p.network_object.id)
	end
end

return pt
