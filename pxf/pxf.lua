package.path = package.path .. "./pxf/?.lua;"

require("shader")
require("geometry")
require("texture")
require("framebuffer")
require("quadbatch")

pxf 			= {}

pxf.shader 		= __shader
pxf.geometry 	= __geometry
pxf.texture 	= __texture
pxf.framebuffer = __framebuffer
pxf.quadbatch 	= __quadbatch

return pxf