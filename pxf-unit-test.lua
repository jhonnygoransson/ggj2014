package.path = package.path .. "./ufo/?.lua;"
package.path = package.path .. "./pxf/?.lua;"

local ffi   = require( "ffi" )
local gl   	= require( "ffi/OpenGL" )
local glfw  = require( "ffi/glfw" )
local pxf 	= require( "pxf" )

assert( glfw.glfwInit() )

local window = glfw.glfwCreateWindow( 1024, 768, glfw.GLFW_WINDOWED, "Spinning Triangle", nil)

assert( window )
glfw.glfwMakeContextCurrent( window )
glfw.glfwSetInputMode(window, glfw.GLFW_STICKY_KEYS, 1)
glfw.glfwSwapInterval(1)

print("\nPXF Framework test")
print("##################\n")

print("SHADERS:\n")
print("Building shader from raw source")

test_vs 	= {}
table.insert(test_vs, "attribute vec3 position;")
table.insert(test_vs, "attribute vec3 normal;")
table.insert(test_vs, "attribute vec3 uv0;")
table.insert(test_vs, "uniform mat2 mat2test;")
table.insert(test_vs, "uniform mat3 mat3test;")
table.insert(test_vs, "uniform mat4 mat4test;")
table.insert(test_vs, "varying vec3 vNor;")
table.insert(test_vs, "void main(void) {")
table.insert(test_vs, "  vNor = normal;")
table.insert(test_vs, "  vec2 mat2t_v = mat2test * vec2(0.0);")
table.insert(test_vs, "  vec3 mat3t_v = mat3test * vec3(0.0);")
table.insert(test_vs, "  gl_Position = mat4test * vec4(mat2t_v,uv0) + vec4(mat3t_v,0.0) + vec4(position,1.0);")
table.insert(test_vs, "}")
test_vs 	= table.concat(test_vs)

test_fs 	= {}
table.insert(test_fs, "varying vec3 vNor;")
table.insert(test_fs, "uniform float cool;")
table.insert(test_fs, "uniform vec3 coolv;")
table.insert(test_fs, "void main(void) {")
table.insert(test_fs, "  gl_FragColor = vec4( vNor ,cool + coolv.x);")
table.insert(test_fs, "}")
test_fs 	= table.concat(test_fs)

testshader = pxf.shader( test_vs, test_fs )

testshader:bind()

print( "\nget location:")
print( "  attrib.position.loc", testshader:getattriblocation("position") )
print( "  attrib.normal.loc", testshader:getattriblocation("normal") )
print( "  uniform.cool.loc", testshader:getuniformlocation("cool") )
print( "\nset uniforms:")
print( "  uniform.cool    ", testshader:setuniform("f","cool", 1.0 ) )
print( "  uniform.coolv   ", testshader:setuniform("fv","coolv", { 1.0, 1.0, 1.0 } ) )
print( "  uniform.mat2test", testshader:setuniform("mat2","mat2test", { 1.0, 1.0, 1.0, 1.0 } ) )
print( "  uniform.mat3test", testshader:setuniform("mat3","mat3test", { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 } ) )
print( "  uniform.mat4test", testshader:setuniform("mat4","mat4test", { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 } ) )

testshader:unbind()

print("\nBuilding shader from external source")
extshader = pxf.shader("data/shader/unit-test.vs","data/shader/unit-test.fs",true)
print("  Building shader ", "OK")
extshader:reload()
print("  Reloading shader", "OK")


print("\nGEOMETRY:\n")

local fs_pos = { 

  -- ----/
  -- |  /
  -- | /
  -- |/

  -0.5, 0.5, 0,1,
   0.5, 0.5, 0,1,
  -0.5,-0.5, 0,1,

  --    /|
  --   / |
  --  /  |
  -- /----

  -0.5,-0.5, 0,1,
   0.5,-0.5, 0,1,
   0.5, 0.5, 0,1 
}; 

local fs_uv = {
    0,0,
    1,0,
    0,1,

    0,1,
    1,1,
    1,0
};

geo_params = { 
      buffers = {
        position = {
          data = fs_pos,
          size = 4
      }, 
        uv0 = {
        data = fs_uv,
        size = 2
      }
    } 
  }


testgeo = pxf.geometry( geo_params )

print("Build from RAW data", "OK, geometry has " .. testgeo.drawlength .. " primitives")

testgeo:bind( testshader:bind(), { "position" } )

print("Binding buf.position", "OK")

testgeo:bind( testshader:bind(), { "uv0" } )
print("Binding buf.uv      ", "OK")

testgeo:draw()
print("Drawing geometry", "OK")

testgeo:unbind()
print("Unbinding all       ", "OK")

bingeo = pxf.geometry( { buffers = { url = "sphere.bin" } }, "data/mesh/")

print("Building from external", "OK")

bingeo:destroy()

print("Destroying           ", "OK")
print("Build                 ", "OK")

print("\nTEXTURE:\n")

texture = pxf.texture( "data/test.png", { magfilter = gl.GL_LINEAR, minfilter = gl.GL_LINEAR } )

print("Build from path    ", "OK")

texture:bind()

print("Bind              ", "OK")

texture:unbind()

print("Unbind            ", "OK")

texture:destroy()

print("Destroy          ", "OK")

-- print("binary!")
-- print( bunpack( bpack("f",95.0), "f") )

print("\nFRAMBUFFER:\n")

fbo = pxf.framebuffer()
print("Build                 ", "OK")

print("Status                ", "OK", fbo:status() and fbo.status )

fbo:bind()
print("Bind                  ", "OK")

local texid = ffi.new( "GLint[1]" )
gl.glGenTextures(1,texid)

fbo:attachcolortexture( 1, texid[0] )
print("Attachcolortexture    ", "OK")

fbo:detachbyindex(1)
print("Detachbyindex         ", "OK")

fbo:unbind()
print("Unbind                ","OK")

fbo:delete()
print("Delete               ","OK")


print("\nQUADBATCH:\n")

qb = pxf.quadbatch()

print("Crete new           ", "OK")

qb:begin()

print("Begin               ", "OK")

qb:add( 0,0, 1,0, 1,1, 0,1 )

print("Add                 ", "OK")

qb:addcentered( 0,0,1,1 )
print("Add centered       ", "OK")

qb:addtopleft( 0,0,1,1 )
print("Add topleft       ", "OK")

qb:finish()
print("Finish              ", "OK", qb.drawlength .. " primitives")

qb:draw( testshader )

print("Draw                ", "OK")

qb:destroy()

print("Destroy             ", "OK")

print("All OK")