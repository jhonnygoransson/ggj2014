local ffi 	= require("ffi")
local gl 	= require( "ffi/OpenGL" )

__texture = function( path, params )
	local   t  					= {}
	        t.minfilter     	= gl.GL_LINEAR
	        t.magfilter     	= gl.GL_LINEAR
	        t.datatype      	= gl.GL_UNSIGNED_BYTE
	        t.internalformat    = gl.GL_RGBA
	        t.format            = gl.GL_RGBA
	        t.wraps             = gl.GL_CLAMP_TO_EDGE
	        t.wrapt             = gl.GL_CLAMP_TO_EDGE
	        t.path              = path
	        t.target            = gl.GL_TEXTURE_2D
	        t.type              = "texture"

	extend( t, params )

	t.build  = function(self, path)
        if path then 
            self:Destroy()
            self.path = path
        end

        -- if not self.handle then
            -- self.handle = gl.GenTextures(1)[1]
        -- end

        if self.path then
            print("Loading texture from path: " .. self.path)

            self.handle, self.width, self.height = pxfaux.tex_load(self.path)

            -- load from path
            -- local d,w,h,bpp = FreeImage.load(inp)
            -- self.bpp                = bpp

            self:upload()
        end

        return self
    end
	
    t.bind  = function(self,unit)
        if ( self.handle == nil ) then return end
    	if ( unit == nil ) then unit = 0 end 

        gl.glActiveTexture( gl.GL_TEXTURE0 + unit )
        gl.glBindTexture( self.target, self.handle )

        self.unit = unit
    end

    t.unbind = function(self)
        if self.unit == nil then return end 

        gl.glActiveTexture(gl.GL_TEXTURE0 + self.unit)
        gl.glBindTexture( self.target, 0)
    end

    t.upload = function(self, force_data )
        -- self.w = w
        -- self.h = h

        self:bind(0)

        if force_data then
            gl.glTexImage2D( gl.GL_TEXTURE_2D, 0, self.internalformat, self.width, self.height, 0,self.format, self.datatype, force_data )
        end

        -- gl.TexImage2DRaw( 0, self.internalformat, self.w,self.h, 0, self.format, self.datatype, data )

        gl.glTexParameteri(gl.GL_TEXTURE_2D,gl.GL_TEXTURE_WRAP_S, self.wraps )
        gl.glTexParameteri(gl.GL_TEXTURE_2D,gl.GL_TEXTURE_WRAP_T, self.wrapt )

        gl.glTexParameteri(gl.GL_TEXTURE_2D,gl.GL_TEXTURE_MIN_FILTER, self.minfilter )
        gl.glTexParameteri(gl.GL_TEXTURE_2D,gl.GL_TEXTURE_MAG_FILTER, self.magfilter )

        -- todo : mipmaps..
        self:unbind()

        return self
    end

    t.uploadempty = function(self,w,h)
            self.width  = w
            self.height = h

            print(w,h)

            self:bind(0)

            gl.glTexImage2D( gl.GL_TEXTURE_2D, 0, self.internalformat, w, h, 0,self.format, self.datatype, nil)

            gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_WRAP_S, self.wraps )
            gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_WRAP_T, self.wrapt )

            gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MIN_FILTER, self.minfilter )
            gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MAG_FILTER, self.magfilter )

            -- todo : mipmaps..
            self:unbind()

            return self
    end
  
    t.destroy = function(self)
        if self.handle == nil then return self end

    	local ptr = ffi.new("const GLuint[1]", self.handle )
        gl.glDeleteTextures( 1, ptr )
        self.handle = nil

        return self
    end

    return t:build()
end