local gl            = require( "ffi/OpenGL" )
local pxf           = require('pxf')
local spritemanager = require("game/spritemanager")

local KEY_PRESS   = 0
local KEY_ON      = 1
local KEY_RELEASE = 2

local gamescreen = {}
gamescreen.inputstate = {}

physics = nil
player_id = nil

function gamescreen:updateinput(game)
	for k,v in pairs(self.inputstate) do
		if v == KEY_RELEASE then
			self.inputstate[k] = nil
		elseif v == KEY_PRESS then
			self.inputstate[k] = KEY_ON
		end
	end	

	repeat
		local ev = game:popevent()
		if ev ~= nil then
			if ev.key == "ESC" and ev.action == "OFF" then
				if (game.hosting) then
					client:clean_up()
					server:clean_up()
				end
				game:pop_screen()
			else
				client:send("keyboard", {key = ev.key, action = ev.action})
			end
			--	if ( ev.key ) then
			--		 self.inputstate[ev.key] = KEY_RELEASE
			--	else self.inputstate[ev.code] = KEY_RELEASE end
			--elseif ev.action == "ON" then
			--	if ( ev.key ) then
			--		 self.inputstate[ev.key] = KEY_PRESS
			--	else self.inputstate[ev.code] = KEY_PRESS end
			--end
		end
	until ev == nil
end

function event_callback(message_type, message)
	-- print(message_type, message)
	if message_type == "player_id" then
		player_id = message
	end
end

function connect_callback()
	print("connected.")
end

function disconnect_callback(err)
	print("disconnected:",err)
	client:clean_up()
	reconnect()
end

reconnections = 0
function reconnect()
	if reconnections > 20 then
		game:pop_screen()
		return
	end
	client = coolnet.create_client(arg[1], arg[2], event_callback,
		connect_callback, disconnect_callback)
	reconnections = reconnections + 1
end

function gamescreen:init(game)
	self.game         = game
	self.scale        = 1
	self.map 	      = require("game/map"):init("data/maps/grassy")
	self.camera       = require("game/camera")
	self.spriteshader = assets["spriteshader"]
	self.camera:set_gameobject(game)
	self.camera:look_at(0,0)
	self.map:set_tiletexture(assets["tile_texture"])

	self.spritesheets = {}
	self.spritesheets["player"] = spritemanager:new(assets["spritesheet"], {64,96})
	self.spritesheets["player"]:create(10)
	self.spritesheets["player"]:animation( "walk", 1/24, {{0,0}} )

	if game.hosting then
		if (package.loaded["game/physics"]) then
            package.loaded["game/physics"] = nil
        end
		physics = require("game/physics")
		physics.init(arg[1], arg[2])

		local map_layer = self.map.map.layers[1]
		local tile_set = self.map.map.tilesets[1]
		for i=1,#map_layer.data do
			tile_idx = map_layer.data[i]
			if tile_idx ~= 0 then
				local x = ((i - 1) % map_layer.width) -- +1, since index starts at 1
				local y = (math.floor((i-1) / map_layer.width)) -- dito

				physics.new_platform(x, y, 1, 1)
			end
		end
	else
		physics = nil
	end
	client = coolnet.create_client(arg[1], arg[2], event_callback,
		connect_callback, disconnect_callback)
end

function gamescreen:update(game,dt,vis)
	self:updateinput(game)

	if self.inputstate["ESC"] then
		game:pop_screen()
	end

	--if self.inputstate["LEFT"] then
	--	self.camera:translate(-10 * dt,0)
	--end
	--if self.inputstate["RIGHT"] then
	--	self.camera:translate(10 * dt,0)
	--end

	--if self.inputstate["DOWN"] then
	--	self.camera:translate(0,10 * dt)
	--end
	--if self.inputstate["UP"] then
	--	self.camera:translate(0,-10 * dt)
	--end

	if self.inputstate[string.byte("S")] then
		self.scale = self.scale - 1 * dt
	end

	if self.inputstate[string.byte("W")] then
		self.scale = self.scale + 1 * dt
	end

	self.scale = math.max( self.scale, 0.1 )

	self.camera:zoom(self.scale)
	self.camera:update(dt)
	if game.hosting then
		physics.update(dt)
	end
	if client then
		client:update(dt)
		if (client.world_classes["player"]) then
			local idx = 0
			for k,v in pairs(client.world_classes["player"]) do
				local x, y = v:get_field("x"), v:get_field("y")
				
				if x == nil then x = 0 end
				if y == nil then y = 0 end
				if v.id == player_id then
					self.camera:look_at(x,y)
				end
				self.spritesheets["player"]:update(idx,0,{x,y,0,0})
				idx = idx + 1
			end
		end
	end
end

function gamescreen:draw(game,vis)
	-- frame setup
	gl.glViewport(0, 0, game.settings.win_w, game.settings.win_h)
	gl.glClearColor(0.8125, 0.953125, 0.96484375, 1.0)
	gl.glClear( gl.GL_COLOR_BUFFER_BIT )
	gl.glDisable( gl.GL_DEPTH_TEST )

	-- draw background layer
	-- draw map layer
	self.map:draw(self.camera)

	-- draw entity layer
	self.spriteshader:bind()
	self.spriteshader:setuniform( "mat4", "pmatrix", self.camera:get_projection())
	self.spriteshader:setuniform( "f", "batches", 1 )
	self.spritesheets["player"]:draw( self.spriteshader,{ position = true, uv0 = true, normal = false })
	self.spriteshader:unbind()

	-- draw foreground layer
	-- draw transparent layer
end

return gamescreen
