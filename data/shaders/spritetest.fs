uniform sampler2D tex0;
uniform sampler2D tex1;

uniform float batches;

varying vec2 vUv;
varying float vTileIndex;

void main()
{
	vec4 spritedata = texture2D( tex1, vec2(vUv.s, 1.0 - vUv.t) );
	gl_FragColor 	= spritedata;
}
