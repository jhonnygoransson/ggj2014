local ffi  = require( "ffi" )
local glfw = require( "ffi/glfw3" )
local gl   = require( "ffi/OpenGL" ) 
local pxf   = require( "pxf/pxf" ) 
local gamefont   = require( "pxf/gamefont" ) 


require("game/spritemanager")
require("pxf/glmatrix")
--require 'text'

shader = pxf.shader("data/shaders/preload.vs", "data/shaders/preload.fs", true)

progress_quad   = nil

function ext_buffer(url) return { buffers = {url = url}} end

local preload_screen = {}
assets = {
	font_texture = pxf.texture("data/textures/bitmapfont.png"),
	font_batch   = pxf.quadbatch(),
	font_shader  = pxf.shader("data/shaders/bitmapfont.vs", "data/shaders/bitmapfont.fs", true)
} -- fill er up
preload_screen.assets_list = {
			tile_texture = {
				load = pxf.texture,
				args = {
					"data/textures/tiles_spritesheet.png",
					{ minfilter = gl.GL_NEAREST, magfilter = gl.GL_NEAREST}
				}
			},
			spriteshader = {
				load = pxf.shader,
				args = {
				 	"data/shaders/spritetest.vs",
				 	"data/shaders/spritetest.fs",
				 	true
				}
			},
			spritesheet = {
				load = pxf.texture,
				args = {
					"data/textures/player1.png",
					{minfilter = gl.GL_NEAREST, magfilter = gl.GL_NEAREST}
				}
			}
			--[[pipe_mdl		= { load = pxf.geometry, args = { ext_buffer("data/models/pipe.bin"), "" } }, 
			pipe_tex 		= { load = pxf.texture, args = { "data/textures/flute_diffuse.png", {wraps = gl.GL_REPEAT, wrapt = gl.GL_REPEAT} } },
			pipe_shader 	= { load = pxf.shader, args = { "data/shaders/pipe.vs", "data/shaders/pipe.fs", true }},
			pipe_hull_mdl	= { load = pxf.geometry, args = { ext_buffer("data/models/hull.bin"), "" } }, 
			pipe_hull_tex 	= { load = pxf.texture, args = { "data/textures/flute_hull.png" } },
			checker 		= { load = pxf.texture, args = { "data/textures/checker.png", {wraps = gl.GL_REPEAT, wrapt = gl.GL_REPEAT} } },
			
			logo_texture	= { load = pxf.texture, args = { "data/textures/panfluteherologga.png" } },
			logo_qb			= { load = pxf.quadbatch, args = {} },
			logo_shader		= { load = pxf.shader, args = { "data/shaders/logo.vs", "data/shaders/logo.fs", true} },

			font_texture	= { load = pxf.texture, args = { "data/textures/fontN.png" } },
			font_batch		= { load = pxf.quadbatch, args = {} },
			font_shader		= { load = pxf.shader, args = { "data/shaders/font.vs", "data/shaders/font.fs", true} },
			
			-- background-stuff
			bg_sky = { load = pxf.texture, args = { "data/textures/bg_sky.png", {wraps = gl.GL_REPEAT, wrapt = gl.GL_REPEAT} } },
			bg_desert = { load = pxf.texture, args = { "data/textures/bg_desert.png", {wraps = gl.GL_REPEAT, wrapt = gl.GL_REPEAT} }},

			-- game art
			sprite_fj1 	= { load = pxf.texture, args = { "data/textures/sprite_fj1.png" } },
			sprite_fj2 	= { load = pxf.texture, args = { "data/textures/sprite_fj2.png" } },
			sprite_fj3 	= { load = pxf.texture, args = { "data/textures/sprite_fj3.png" } },
			sprite_fj3 	= { load = pxf.texture, args = { "data/textures/sprite_fj4.png" } },
			sprite_sol 	= { load = pxf.texture, args = { "data/textures/sprite_sol.png" } },
			sprite_indian1 	= { load = pxf.texture, args = { "data/textures/sprite_indian1.png" } },
			]]
		}

			-- texthets
			--[[
			sprite_pantastic 	= { load = pxf.texture, args = { "data/textures/sprite_pantastic.png" } },
			sprite_yousuck 	    = { load = pxf.texture, args = { "data/textures/sprite_yousuck.png" } },
			sprite_wow 	        = { load = pxf.texture, args = { "data/textures/sprite_wow.png" } },
			sprite_ugh 	        = { load = pxf.texture, args = { "data/textures/sprite_ugh.png" } },
			sprite_let_it_rain 	= { load = pxf.texture, args = { "data/textures/sprite_let_it_rain.png" } },
			sprite_blowhard 	= { load = pxf.texture, args = { "data/textures/sprite_you_blowhard.png" } },
			sprite_mindblowing 	= { load = pxf.texture, args = { "data/textures/sprite_mindblowing.png" } },
			sprite_blowing_it 	= { load = pxf.texture, args = { "data/textures/sprite_blowing_it.png" } }
			]]--


preload_screen.init = function ( self, game )
	print "init - preload"
end


preload_screen.state  = "init" -- "init" -> "assets" -> "done"
preload_screen.assets_procent   = 0.0
preload_screen.update = function ( self, game, dt, vis )
	
	repeat
        local ev = game:popevent()
        if ev ~= nil then
            print("preload event", ev.code, ev.key, ev.action)
            if ev.action == "OFF" then
            	if ev.key == "ESC" then
            		game.running = false
            	end
            end
        end
    until ev == nil

	-- init spotify
	if (self.state == "init") then
		
		self.state = "assets"
		table.insert(self.preload_string_history, 1, "Doing tricks on the street... Done")

		local q_w = 400
		local q_h = 12
		local q_x0 = game.settings.win_w / 2 - q_w / 2
		local q_y0 = game.settings.win_h / 2 + q_h / 2
		local q_x1 = game.settings.win_w / 2 + q_w / 2
		local q_y1 = game.settings.win_h / 2 - q_h / 2
		progress_quad   = pxf.geometry( {
			buffers = {
				position = {
					size = 4,
					data = { 0, q_y0, 0, 0,
							 q_w, q_y1, 0, 0,
							 0, q_y1, 0, 0,

							 0, q_y0, 0, 0,
							 q_w, q_y0, 0, 0,
							 q_w, q_y1, 0, 0
					}
				}
			}
		})

	elseif (self.state == "assets") then

		-- print("Preloading gl-assets")
		local all_loaded = true

		local total_asset_count = 0
		for k,v in pairs(self.assets_list) do
			total_asset_count = total_asset_count + 1
		end

		local assets_loaded = 0
		for k,v in pairs(self.assets_list) do 
			if v.done == nil or v.done == false then
				all_loaded = false
				print("Loading: ".. tostring(k))
				assets[k] = v.load(unpack(v.args))
				self.assets_list[k].done = true

				break
			else
				assets_loaded = assets_loaded + 1
			end
		end

		self.assets_procent = assets_loaded / total_asset_count

		if (false or all_loaded) then

			table.insert(self.preload_string_history, 1, "Loading game assets: 100% Done")

			--game:push_screen( require("game/gameplay") )
			--game:push_screen( require("game/tracklist") )
			--game:push_screen( require("game/splash") )
			--game:push_screen( require("game/splash") )
			-- game:push_screen( require("game/debug_network") )
			--game:push_screen( require("game/spritetest") )
			game:push_screen( require("game/mainmenu") )
			-- game:push_screen( require("game/debug_screen") )
			-- game:push_screen( require("game/gamescreen") )
			-- game:push_screen( require("game/physics_test") )
			self.state = "done"

		end
		--assets.preload_done = true

	end

end

preload_screen.preload_string_history = {}
preload_screen.draw = function ( self, game, vis )	

	if (vis < 1) then
		return
	end

	gl.glDisable( gl.GL_DEPTH_TEST )
	gl.glViewport(0, 0, game.settings.win_w, game.settings.win_h)
	gl.glClear( gl.GL_COLOR_BUFFER_BIT )
	gl.glEnable( gl.GL_BLEND )
	gl.glBlendFunc( gl.GL_SRC_ALPHA, gl.GL_ONE_MINUS_SRC_ALPHA )
	gl.glBlendEquation( gl.GL_FUNC_ADD )

    local start_y = game.settings.win_h / 2 - 24 - 12
    for k,v in pairs(self.preload_string_history) do
    	gamefont.draw( game.settings.win_w / 2 - 200, start_y, 16, v, nil, vis)
    	start_y = start_y - 12
    end

    local progress_string = ""
    local preload_string = ""

    if (self.state == "init") then
    	preload_string = "Doing tricks on the street..."
    elseif (self.state == "assets") then
    	progress_string = tostring(math.floor(self.assets_procent * 100)) .. "%"
    	preload_string = "Loading game assets: " .. progress_string
    elseif (self.state == "done") then
    	preload_string = "Full speed ahead!!!"
    end

    gamefont.draw( game.settings.win_w / 2 - 200, game.settings.win_h / 2 - 24, 16, preload_string, nil, vis)


	-- progress bar hack

	shader:bind()
	shader:setuniform("mat4", "pmtx", mat4.ortho(0, game.settings.win_w, game.settings.win_h, 0, 0, 1))
	shader:setuniform("f", "pos_x", game.settings.win_w / 2 - 400 / 2)

	-- draw progress bg
	shader:setuniform("f", "download", 1.0)
	shader:setuniform("fv", "color", {0.12, 0.12, 0.12})
	progress_quad:bind(shader)
	progress_quad:draw()
	progress_quad:unbind()

	-- draw progress bar
	--shader:setuniform("f", "download", self.download_procent * 0.5 + self.assets_procent * 0.5)
	shader:setuniform("fv", "color", {1.0, 1.0, 1.0})
	progress_quad:bind(shader)
	progress_quad:draw()
	progress_quad:unbind()

	shader:unbind()


end


return preload_screen

