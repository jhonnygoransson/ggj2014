local ffi        	= require( "ffi" )
local glfw       	= require( "ffi/glfw3" )
local gl         	= require( "ffi/OpenGL" ) 
local pxf        	= require( "pxf/pxf" )

local megaman 		= spritemanager:new( "data/textures/sprite_test2.png", 32 )
local testbatch 	= spritemanager:new( "data/textures/sprite_test1.png", 16 )
local testshader 	= pxf.shader("data/shaders/spritetest.vs", "data/shaders/spritetest.fs", true)
local batches 		= 4

testbatch:create(batches)

spritetest = {}
fsquad = pxf.geometry( {
			buffers = {
				position = {
					size = 3,
					data = { 
						-1, 1, -1,
						 1, 1, -1,
						-1,-1, -1,
						-1,-1, -1,
						 1,-1, -1,
						 1, 1, -1,
					}
				},
				uv0 = {
					size = 2,
					data = {
						0,0,
						1,0,
						0,1,

						0,1,
						1,1,
						1,0
					}
				}
			}
		})

local state = {}

state[1] = { 0, 3 }
state[2] = { 1, 2 }
state[3] = { 2, 1 }
state[4] = { 3, 0 }

spritetest.__update = function(self)
	testbatch:update( 0,0, { 0, 32,  math.floor(state[1][1]),math.floor(state[1][2]) } )
	testbatch:update( 1,0, { 0, 64,  math.floor(state[2][1]),math.floor(state[2][2]) } )
	testbatch:update( 2,0, { 0, 128, math.floor(state[3][1]),math.floor(state[3][2]) } )
	testbatch:update( 3,0, { 0, 196, math.floor(state[4][1]),math.floor(state[4][2]) } )

	state[1][1] = ( state[1][1] + 0.001 ) % 3
	state[2][1] = ( state[2][1] + 0.001 ) % 3
	state[3][1] = ( state[3][1] + 0.001 ) % 3
end


mm_sprite = megaman:create(1)

local freq = 1/2

local animation = function( sprite,x,y, ... )
	local a      = {}
	local frames = {}
	for k,v in pairs(...) do 
		table.insert(frames,v) 
	end

	print("new animation w/ " .. #frames.. " frames")

	a.frame  = -1
	a.time   = freq
	a.frames = frames
	a.sprite = { sprite, x, y }
	a.next 	 = function(self,dt)
		if self.time >= freq then
			self.frame = ( self.frame + 1 ) % #self.frames
			self.time  = 0

			local framex = self.frames[ self.frame+1][1]
			local framey = self.frames[ self.frame+1][2]

			self.sprite[1]:update( self.sprite[2], self.sprite[3], { 0,0,framex,framey } )
		end

		self.time = self.time + dt
	end

	return a
end

mm_sprite.animdata         = {}
mm_sprite.animdata["idle"] = animation( mm_sprite, 0, 0, { { 2,1 }, { 2,1 }, { 2,1 }, {0,3} } )
mm_sprite.animdata["run"]  = animation( mm_sprite, 0, 0, { {0,0},{1,0},{2,0},{0,0} } )
mm_sprite.animdata["gun"]  = animation( mm_sprite, 0, 0, { {0,2},{1,2},{2,2},{3,2} } )
mm_sprite.active           = mm_sprite.animdata["idle"]

-- mm_sprite.animdata["gun"]  = animation( 8,9,10,11 )
-- mm_sprite.animstate        = { active = "idle", time = 0 }

spritetest.update = function(self,game,dt,vis)
	repeat
        local ev = game:popevent()
        if ev ~= nil then
            print( "preload event", ev.code, ev.key, ev.action )
            if ev.action == "OFF" then
            	if ev.key == "ESC" then
            		game.running = false
            	end
            end

            -- print(ev.action)

            if ev.key == "PL1_MIC1" then
            	print("sprite 1")
            	mm_sprite.active = mm_sprite.animdata["idle"]

            elseif ev.key == "PL1_MIC2" then
            	print("sprite 2")
            	mm_sprite.active = mm_sprite.animdata["run"]
            elseif ev.key == "PL1_MIC3" then
            	print("sprite 3")
            	mm_sprite.active = mm_sprite.animdata["gun"]
            end
        end
    until ev == nil

    mm_sprite.active:next(dt)
end

spritetest.draw = function(self)
	gl.glDisable( gl.GL_DEPTH_TEST )
	gl.glDisable( gl.GL_CULL_FACE )
	gl.glViewport( 0, 0, game.settings.win_w, game.settings.win_h )
	gl.glClearColor( 0.5,0.5,0.5,1.0 )
	gl.glClear( gl.GL_COLOR_BUFFER_BIT + gl.GL_DEPTH_BUFFER_BIT )

	testshader:bind()
	testshader:setuniform("mat4", "pmatrix", mat4.ortho(0, game.settings.win_w, 0, game.settings.win_h, 0, 1))
	testshader:setuniform("f", "batches", batches)

	mm_sprite:draw( testshader, { position = true, uv0 = true, normal = false })

	testshader:unbind()
end

return spritetest