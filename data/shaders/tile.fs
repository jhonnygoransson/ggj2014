uniform sampler2D tex0;
//uniform float vis;

varying vec2 v_uv0;
varying vec3 v_color;

void main()
{
	gl_FragColor = texture2D(tex0, v_uv0);

	//gl_FragColor = vec4(v_uv0.xc, v_uv0.y, 0.0, 1.0);
	/*gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);*/
}
