local ffi  = require( "ffi" )
local glfw = require( "ffi/glfw3" )
local gl   = require( "ffi/OpenGL" ) 
local pxf   = require( "pxf/pxf" ) 

require("game/text")

local main_menu = {}

main_menu.init = function ( self, game )
    self.game = game
end

main_menu.update = function ( self, game, dt, visibility )

    if (visibility < 1) then
        return
    end

    local gametype = nil
    repeat
        local ev = game:popevent()
        if ev ~= nil then
            print("main menu event", ev.code, ev.key, ev.action)
            if ev.action == "OFF" then
                if ev.key == "ESC" then
                    game:pop_screen()
                    print("popping mainmenu screen")
                -- tempmenysaker
                elseif ev.key == "LEFT" then
                    --print "Starta snigelspel"
                    --gametype = "snigel"
                elseif ev.key == "UP" then
                    self.current_option = self.current_option - 1
                    if (self.current_option < 1) then
                        self.current_option = #self.options
                    end
                    --print "Starta multispel"
                    --gametype = "multi"
                elseif ev.key == "DOWN" then
                    self.current_option = self.current_option + 1
                    if (self.current_option > #self.options) then
                        self.current_option = 1
                    end
                    --print "Bygg banana"
                    --gametype = "bananbygge"
                elseif ev.key == "ENTER" then

                    print("selecting: " .. self.options[self.current_option].label)

                    if (self.options[self.current_option].knode) then
                        local a = loadstring(self.options[self.current_option].knode)
                        a = setfenv(a, { game = game })
                        a()
                    end

                    if (self.options[self.current_option].screen) then

                        -- make sure old screen is removed
                        if (package.loaded[self.options[self.current_option].screen]) then
                            package.loaded[self.options[self.current_option].screen] = nil
                        end
                        game:push_screen( require(self.options[self.current_option].screen) )

                    end

                end
            end
        end
    until ev == nil
end

main_menu.options = {
    {
        label  = "Join",
        screen = "game/joinscreen"
    },
    {
        label  = "Host",
        screen = "game/gamescreen",
		knode  = "game.hosting = true"
    },
    {
        label = "Quit",
        knode = "game.running = false"
    },
}
main_menu.current_option = 1
main_menu.draw = function ( self, game, vis )
    -- gl.glMatrixMode(gl.GL_PROJECTION);
    -- gl.glLoadIdentity();
    -- gl.glOrtho(0, game.settings.win_w, game.settings.win_h, 0, 0, 1)

    -- gl.glMatrixMode( gl.GL_MODELVIEW );
    -- gl.glLoadIdentity();

    -- local y = 24
    -- for k,v in pairs(self.options) do
    --     local extra_label = "  "
    --     if (self.current_option == k) then
    --         extra_label = "> "
    --     end
    --     draw_string( 24, y, extra_label .. v.label)
    --     y = y + 14
    -- end

    -- draw_flush()

    local y = 24
    for k,v in pairs(self.options) do
        local extra_label = "  "
        if (self.current_option == k) then
            extra_label = "> "
        end
        -- draw_string( 24, y, extra_label .. v.label)
        gamefont.draw( 24, y, 16, extra_label .. v.label, nil, vis)
        y = y + 14
    end

    
end

return main_menu
