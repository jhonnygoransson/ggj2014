box2d = require( "love_physics" )
coolnet = require("game/coolnet")

local ph = {}

local gravitation = 9.82
local acceleration = { x = 5, y = 0 }

local playerwidth = 0.2
local playerheight = 0.2

-- collectgarbage("stop")
ph.world = box2d.newWorld(0, gravitation, false)
ph.world:setMeter( 1 )
ph.players = {}
ph.platforms = {}
--ph.objects = {}

if (not loltable) then
	print("It has been decided")
	loltable = {}
end

spawning_positions = { {2,20}, {3,20}, {4,20}, {5,20}, {6,20}, {7,20} }


function unhandled_handler(client, message_type, message)
	local player = ph.players[client]

	if message_type == "keyboard" then
		-- print("player",player)

		if message.action == "OFF" then
			if message.key == "LEFT" then
				player:goLeft(false)
			elseif message.key == "RIGHT" then
				player:goRight(false)
			end
		elseif message.action == "ON" then
			if message.key == "LEFT" then
				player:goLeft(true)
			elseif message.key == "RIGHT" then
				player:goRight(true)
			elseif message.key == "UP" then
				player:jump()
			end
		end
	else
		print("unhandled type: ", message_type)
	end
end

function client_connected( client )
	ph.new_player(client)
	-- ph.players[client]:setPosition(0, 0)
end

function client_disconnected( client )
	print("player left")
end

function ph.init(host, port)
	server = coolnet.create_server(host, port, unhandled_handler, client_connected, client_disconnected)
end


function find_player_by_body(body)
	for i,v in pairs(ph.players) do
		if v.body == body then
			return v
		end
	end
	return nil
end


function beginContact(a, b, coll)
	table.insert(loltable,coll)

	local adata = a:getUserData()
	local bdata = b:getUserData()
	if adata == "platform" or bdata == "platform" then
		return
	else
		local abody = a:getBody()
		local bbody = b:getBody()
		local ax, ay = abody:getPosition()
		local bx, by = bbody:getPosition()
		if math.abs(ay - by) < playerheight - 10 then
			print("noone died")
			return
		end

		local dead_player = nil
		if ay > by then
			dead_player = find_player_by_body(abody)
		else
			dead_player = find_player_by_body(bbody)
		end

		if dead_player then
			print("player died: ", dead_player)
			server:send_to_all("client_event", { action="death", player=dead_player.network_object.id })

			dead_player.is_dead = true
		end
	end
end

function endContact(a, b, coll)
	table.insert(loltable,coll)
end

ph.world:setCallbacks(beginContact, endContact, nil, nil)


function ph.new_player(client)
	local player = {}

	player.width = playerwidth
	player.height = playerheight
	player.mass = 500

	player.body = box2d.newBody(ph.world, 0, 0, "dynamic")
	player.body:setFixedRotation(true)
	player.body:setMass(player.mass)
	player.rect = box2d.newRectangleShape(0, 0, player.width, player.height, 0.0)
	player.fixture = box2d.newFixture(player.body, player.rect, 1.0)
	player.fixture:setUserData("player")

	player.moving_left = false
	player.moving_right = false

	player.is_dead = false

	function player.getPosition(self)
		return self.body:getPosition()
	end

	function player.getSize(self)
		return self.width, self.height
	end

	function player.setPosition(self, x, y)
		self.body:setX(x)
		self.body:setY(y)
	end

	function player.goLeft(self, b)
		self.moving_left = b
	end

	function player.goRight(self, b)
		self.moving_right = b
	end

	function player.jump(self, force)
		if force == nil then
			force = 0.004
		end

		player.body:applyLinearImpulse(0, -force)
	end

	function player.update(self)
		local x, y = self:getPosition()
		-- print(x,y)
		self.network_object:set_field("x", x)
		self.network_object:set_field("y", y)

		if self.is_dead then
			self.is_dead = false
			self:spawn()
		end
	end

	function player.spawn(self)
		local idx = math.floor(math.random() * #spawning_positions + 1)
		local x,y = unpack(spawning_positions[idx])
		-- print(x,y)
		self:setPosition(x,y)
	end

	player.network_object = server:create_object("player")
	player.network_object:add_field("x", 0, "linear")
	player.network_object:add_field("y", 0, "linear")
	client:send("player_id", player.network_object.id)

	ph.players[client] = player
	player:spawn()
	player:update()
	return player
end

function ph.new_platform(x, y, w, h)
	local platform = {}

	platform.x = x
	platform.y = y
	platform.width = w
	platform.height = h

	platform.body = box2d.newBody(ph.world, x, y, "static")
	platform.rect = box2d.newRectangleShape(platform.width, platform.height)
	platform.fixture = box2d.newFixture(platform.body, platform.rect, 1.0)
	platform.fixture:setUserData("platform")

	table.insert(ph.platforms, platform)
	return platform
end


function ph.update(delta)
	for i, p in pairs(ph.players) do
		p:update()
		-- print(p:getPosition())

		local dx, dy = p.body:getLinearVelocity()
		if p.moving_left and not p.moving_right then
			dx = -acceleration.x
		elseif p.moving_right and not p.moving_left then
			dx = acceleration.x
		elseif not p.moving_right and not p.moving_left then
			dx = 0
		end

		p.body:setLinearVelocity(dx, dy)
	end

	ph.world:update(delta)
	server:update(delta)
end


return ph
