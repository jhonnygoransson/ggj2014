local ffi  = require( "ffi" )
local gl   = require( "ffi/OpenGL" ) 

local floor, rshift, band = math.floor, bit.rshift, bit.band

local font = require( "lib/fonts/pearl8x8" )
local font = ffi.new( "uint8_t[?]", #font, font )

local vbo_capacity = 1024*1024
local vbo_index = 0
local vbo = ffi.new( "float[?]", vbo_capacity )

function draw_char(x,y,c)
   local font, vbo, index = font, vbo, vbo_index
   assert( index + 8 * 8 * 2 < vbo_capacity )
   c = c * 8
   for i=0,7 do
      local b = font[c + i]
      for j=0,7 do
	 if band(rshift(b, 7-j), 1) == 1 then
	    vbo[ index + 0 ] = x + j + 4
	    vbo[ index + 1 ] = y + i + 4
	    index = index + 2
	 end
      end
   end
   vbo_index = index
end

function draw_string(x,y,s)
   local height = 800 --pfhero.win_h
   for i=1,#s do
      local c = s:byte(i)
      if c == 10 or c == 13 then
	 y = y + 10 
	 if y > height then
	    break
	 end
	 x = 0
      elseif c == 9 then
	 x = floor(x/64 + 1) * 64
      else
	 draw_char(x,y,c)
	 x = x + 8
      end
   end
end

function draw_flush()
   gl.glEnableClientState( gl.GL_VERTEX_ARRAY ) 
   gl.glVertexPointer( 2, gl.GL_FLOAT, 0, vbo )
   gl.glDrawArrays( gl.GL_POINTS, 0, vbo_index )
   gl.glDisableClientState( gl.GL_VERTEX_ARRAY )
   vbo_index = 0 
end