local ffi 	= require("ffi")
local gl 	= require( "ffi/OpenGL" )

function __framebuffer()
	local f = {}

	local fbo_ptr 		= ffi.new( "GLint[1]" )
	gl.glGenFramebuffers( 1, fbo_ptr )

	f.handle 			= fbo_ptr[0]
	f.attachments 		= {}
	f.attachments.color = {}
	f.attachments.depth = nil

	function f:status()
		local status = gl.glCheckFramebufferStatus( gl.GL_FRAMEBUFFER )

		if status == gl.GL_FRAMEBUFFER_COMPLETE then
			self.status = "Framebuffer complete"
			return true
		elseif status == gl.GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT then
			self.status = "Incomplete framebuffer: FRAMEBUFFER_INCOMPLETE_ATTACHMENT"
		elseif status == gl.GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT then
			self.status = "Incomplete framebuffer: FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT"
		elseif status == gl.GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS then
			self.status = "Incomplete framebuffer: FRAMEBUFFER_INCOMPLETE_DIMENSIONS"
		elseif status == gl.GL_FRAMEBUFFER_UNSUPPORTED then
			self.status = "Incomplete framebuffer: FRAMEBUFFER_UNSUPPORTED"
		elseif status == gl.GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER then
			self.status = "Incomplete framebuffer: FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER"
		elseif status == gl.GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER then
			self.status = "Incomplete framebuffer: FRAMEBUFFER_INCOMPLETE_READ_BUFFER"
		elseif status == gl.GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE then
			self.status = "Incomplete framebuffer: FRAMEBUFFER_INCOMPLETE_MULTISAMPLE"
		elseif status == gl.GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS then
			self.status = "Incomplete framebuffer: FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS"
		else 
			self.status = "Incomplete framebuffer: " .. status
		end

		return false
	end

	function f:GetAttachment(i)
		if not i then i = 1 end
		if not self.attachments.color[i] then return -1
		else return self.attachments.color[i] end
	end

	function f:bind()
		if self.bound then
			return
		end

		gl.glBindFramebuffer(gl.GL_FRAMEBUFFER,self.handle)

		self.bound = true
	end

	function f:unbind()
		gl.glBindFramebuffer(gl.GL_FRAMEBUFFER,0)
		self.bound = false
	end

	function f:attachcolortexture(tex,index)
		local attachmentIndex = index and index or #self.attachments.color + 1

		self.attachments.color[attachmentIndex] = tex

		local glAttachmentIndex = attachmentIndex - 1 

		gl.glFramebufferTexture2D( gl.GL_FRAMEBUFFER, gl.GL_COLOR_ATTACHMENT0 + glAttachmentIndex, gl.GL_TEXTURE_2D, tex, 0)
	end

	function f:attachdepthtexture(tex)
		self.attachments.depth = tex

		gl.glFramebufferTexture2D( gl.GL_FRAMEBUFFER, gl.GL_DEPTH_ATTACHMENT, gl.GL_TEXTURE_2D, tex, 0)
	end

	function f:attachdepthbuffer(buf)
		self.attachments.depth = buf

		gl.glFramebufferRenderbuffer(gl.GL_FRAMEBUFFER, gl.GL_DEPTH_ATTACHMENT, gl.GL_RENDERBUFFER, buf)
	end

	function f:detachbyindex(ix) 

		if not self.attachments.color[ix] then
			print("No texture attached at " .. ix)
			return
		end

		local glix = ix -1 

		gl.glFramebufferTexture2D(gl.GL_FRAMEBUFFER,gl.GL_COLOR_ATTACHMENT0 + glix, gl.GL_TEXTURE_2D, 0, 0)
		self.attachments.color[ix] = nil
	end

	function f:delete()
		local ptr = ffi.new("const unsigned int[1]",self.handle)

		gl.glDeleteFramebuffers(1, ptr )
	end

	return f
end