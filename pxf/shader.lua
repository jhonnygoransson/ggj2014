local ffi = require("ffi")
local gl = require( "ffi/OpenGL" )
local u = require("utils")

function shaerr( shader, self )
   local int 	= ffi.new( "GLint[1]" )
   gl.glGetShaderiv( shader, gl.GL_INFO_LOG_LENGTH, int )
   local length = int[0]

   if length <= 0 then
      return
   end

   gl.glGetShaderiv( shader, gl.GL_COMPILE_STATUS, int )
   local success = int[0]
   if success == gl.GL_TRUE then
      return
   end

   local buffer = ffi.new( "char[?]", length )
   gl.glGetShaderInfoLog( shader, length, int, buffer )

   print(ffi.string(buffer))
   print("shader vs: " .. tostring(self.vsraw))
   print("shader fs: " .. tostring(self.fsraw))
end

__shader = function( vs,fs,extern )
	local s 			= {}

	-- data members
	s.vsraw 			= vs
	s.fsraw 			= fs
	s.extern 			= extern
	s.attribs 			= {}
	s.uniforms 			= {}
    s.err               = {}

	-- opengl members
	s.vs            	= gl.glCreateShader(gl.GL_VERTEX_SHADER)
    s.fs            	= gl.glCreateShader(gl.GL_FRAGMENT_SHADER)
    s.program       	= gl.glCreateProgram()

    s.init = function(self)
    	if ( not self.extern ) then 
    			return self:initfromraw( self.vsraw, self.fsraw )
    	else 	return self:reload( self.vsraw, self.fsraw ) end
	end

    -- initialize from raw data
    s.initfromraw 		= function(self, _vs, _fs)
        --self.vs_ptr         = ffi.new( "char[?]", #_vs, _vs )
    	self.vs_ptr 		= ffi.new( "char*", ffi.cast( "char*", _vs) )
        --self.fs_ptr         = ffi.new( "char[?]", #_fs, _fs )
		self.fs_ptr 		= ffi.new( "char*", ffi.cast( "char*", _fs) )

	    -- build shader sources
	    gl.glShaderSource( self.vs,1, ffi.new( "const char*[1]", self.vs_ptr ), nil ); 
        gl.glShaderSource( self.fs,1, ffi.new( "const char*[1]", self.fs_ptr ), nil );

	    gl.glCompileShader( self.vs ); shaerr(self.vs, self)
	    gl.glCompileShader( self.fs ); shaerr(self.fs, self)

	    gl.glAttachShader( self.program, self.vs)
        gl.glAttachShader( self.program, self.fs)
        gl.glLinkProgram( self.program)

	    return self
	end

	-- reload from path 
    s.reload = function(self, _vspath, _fspath )
    	if ( not _vspath or not _fspath ) then
    		_vspath = self.vsrawpath; _fspath = self.fsrawpath;
    	end

    	if ( _vspath and _fspath ) then
    		self.extern = true

    		local _vs,_e = io.open(_vspath,"r")
    		if ( _e ) then print("Shader error when reading " .. _vspath .. ", reason:\n\t" .. _e); return self end;

    		local _fs,_e = io.open(_fspath,"r")
    		if ( _e ) then print("Shader error when reading " .. _vspath .. ", reason:\n\t" .. _e); return self end;

            self.vsraw = _vs:read("*a")
            _vs:close()
            self.fsraw = _fs:read("*a")
            _fs:close()

            self._vspathpath = _vs 
            self._fspathpath = _fs

    		return self:initfromraw(self.vsraw, self.fsraw)
    	end

    	return self
	end	


	s.bind = function(self)
		gl.glUseProgram(self.program)

        return self
	end

	s.unbind = function(self)
		gl.glUseProgram(0)

        return self
	end

	s.getattriblocation = function( self,name )
        if ( name == nil ) then 
            self.err["Nil passed to getattriblocation"] = true
            return -1
        end

		if ( self.attribs[name] ) then
			return self.attribs[name]
		end

		local loc 			= gl.glGetAttribLocation(self.program,name)
		self.attribs[name] 	= loc

		return loc
	end

	s.getuniformlocation = function(self, name)
        if self.uniforms[name] then
            return self.uniforms[name]
        end

        local loc = gl.glGetUniformLocation(self.program,name)

        self.uniforms[name] = loc

        return loc
    end

    function s:vertexattrib(attrType, name, size, type, normalized, stride)
        local loc = self:getattriblocation(name)

        if attrType == "f" then

        elseif attrType == "f1" then

        elseif attrType == "f2" then

        elseif attrType == "f3" then

        elseif attrType == "f4" then

        elseif attrType == "p" then
            --index,size,type,normalized,stride, (GLvoid*) offset
            gl.glVertexAttribPointer( loc, size, type, normalized, stride, nil)
        else 
            print("SetVertexAttrib : Unknown attrib type " .. attrType)
        end

    end

    s.setuniform = function(self, typ,name,value,transpose)
        if typ.type == "texture" then 
            typ:bind( value )
            typ = "i"
        end

        local loc = self:getuniformlocation(name)

        if transpose == nil then transpose = false end

        if typ == "f" then
            gl.glUniform1f( loc, value )
        elseif typ == "fv" then
        	if #value == 1 then
        		gl.glUniform1f( loc, value[1] )
        	elseif #value == 2 then
        		gl.glUniform2f( loc, value[1],value[2] )
        	elseif #value == 3 then
        		gl.glUniform3f( loc, value[1],value[2],value[3] )
        	elseif #value == 4 then
        		gl.glUniform4f( loc, value[1],value[2],value[3],value[4] )
        	end
        elseif typ == "i" then
            gl.glUniform1i(loc,value)
        elseif typ == "iv" then
        	if #value == 1 then
        		gl.glUniform1i(loc,value[1])
        	elseif #value == 2 then
        		gl.glUniform2i(loc,value[1],value[2])
        	elseif #value == 3 then
        		gl.glUniform3i(loc,value[1],value[2],value[3])
        	elseif #value == 4 then
        		gl.glUniform4i(loc,value[1],value[2],value[3],value[4])
        	end
        elseif typ =="mat2" then
        	assert( #value >= 4 )

        	local ptr = ffi.new("float[4]")
        	ptr = fillptrfromtable(ptr,value,4)

        	gl.glUniformMatrix2fv(loc,1,transpose, ptr )
        elseif typ == "mat3" then
        	assert( #value >= 9 )

        	local ptr = ffi.new("float[9]")
        	ptr = fillptrfromtable(ptr,value,9)

        	gl.glUniformMatrix3fv(loc,1,transpose, ptr )
        elseif typ == "mat4" then
        	assert( #value >= 16 )

            local ptr = ffi.new("float[16]")
        	ptr = fillptrfromtable(ptr,value,16)

        	gl.glUniformMatrix4fv(loc,1,transpose, ptr )
        end

        return value
    end

    return s:init()
end