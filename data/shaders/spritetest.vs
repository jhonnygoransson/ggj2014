attribute vec3 position;
attribute vec2 uv0;

uniform sampler2D tex0;
uniform sampler2D tex1;

uniform mat4      pmatrix;
uniform vec2      tex0dim;
uniform vec2 	  tiles; // tiles davies tiles the time

varying vec2  vUv;
varying float vTileIndex;

void main()
{
	float s 	= ( position.z - 1.0 ) / tex0dim.s + (1.0 / tex0dim.s )* 0.5;
	float t 	= tex0dim.t * 0.5;

	vec4 s_data = texture2D( tex0, vec2(s,t) );
	vec4 p_data = s_data;

	vec2 fUv 	= vec2(uv0.s,1.0-uv0.t);

	vTileIndex 	= position.z;
	vUv         = ( fUv / tiles ) + ( p_data.zw / tiles );
	gl_Position = pmatrix * vec4( position.xy + p_data.xy, -1.0, 1.0);
}