function string:split(sep)
    local sep, fields = sep or ":", {}
    local pattern = string.format("([^%s]+)", sep)
    self:gsub(pattern, function(c) fields[#fields+1] = c end)
    return fields
end

function deepcopy(orig)
    local orig_type = type(orig)
    local copy
    if orig_type == 'table' then
        copy = {}
        for orig_key, orig_value in next, orig, nil do
            copy[deepcopy(orig_key)] = deepcopy(orig_value)
        end
        setmetatable(copy, deepcopy(getmetatable(orig)))
    else -- number, string, boolean, etc
        copy = orig
    end
    return copy
end

function serialize (tt, indent)

    indent = indent or ""
    if ("table" == type(tt)) then
            local o = { }
            local plain_list = true

            for k,v in pairs(tt) do
                    if ("number" ~= type(k)) then
                            plain_list = false
                            break
                    end
            end

            if (plain_list) then
                    for k,v in pairs(tt) do
                            table.insert(o, indent .. "  " .. serialize(v, indent .. "  "))
                    end
            else
                    for k,v in pairs(tt) do
                            table.insert(o, indent .. "  [\"" .. k .. '\"] = ' .. serialize(v, indent .. "  "))
                    end
            end
            return ("{\n" .. indent .. table.concat(o, ",\n") .. "\n" .. indent .. "}")

    elseif ("number" == type(tt)) then
            return (tostring(tt))

    elseif ("string" == type(tt)) then
            return ('[[' .. tt .. ']]')

    else
            return (tostring(tt))
    end
        
end

--[[
-- http://snippets.luacode.org/?p=snippets/String_to_Hex_String_68
function HexDumpString(str,spacer)
return (
	string.gsub(str,"(.)",
	function (c)
		return string.format("%02X%s",string.byte(c), spacer or "")
	end)
	)
end


local ffi = require("ffi")

ffi.cdef[[
	union _njoy_f { int i; float f; unsigned int u; };
]]

--[[

-- gonna have a bad time..
bpack = function( format, ... )
	print("bpack.format", format )
end

bunpack = function( str, format )
	local reordered = ""

	for i = #str,1,-1 do 
		reordered = reordered .. string.sub( str,i,i )
	end

	-- print( HexDumpString(str), HexDumpString(reordered) )
	-- print("bunpack.format", format, "|" .. str .. "|", #str )

	local s_int 	= tonumber( "0x" .. HexDumpString(reordered), 16 )
	local s_union 	= ffi.new("union _njoy_f", { s_int } )

	if format == "f" then 
		print("float:",s_union.f)
		return nil, s_union.f
	elseif format == "I" then
		print("uint:",s_union.u)
		return nil, s_union.u
	end
end
]]

require("pxfaux")

bpack   = pxfaux.bpack
bunpack = pxfaux.bunpack

---------------------------------------------------------------------
-- Extends a table (object) with the contents of a second table
function extend( obj1, obj2 )
	
	if (type(obj) == "table") then
		error("Parameter 1 to extend is not a table.")
	elseif (type(obj) == "table") then
		error("Parameter 2 to extend is not a table.")
	end

  if obj2 == nil then return end

	for k,v in pairs(obj2) do
		obj1[k] = v
	end

	return obj1

end

function fillptrfromtable(ptr,mtx,elements)
	for i=1,elements do
		ptr[i-1] = mtx[i]
	end

	return ptr
end


function convert_binary_to_lua( data )
    local byte_cnt_lut  = {}
    byte_cnt_lut[0]     = 1
    byte_cnt_lut[1]     = 2
    byte_cnt_lut[2]     = 4
    byte_cnt_lut[3]     = 1
    byte_cnt_lut[4]     = 2
    byte_cnt_lut[5]     = 4
    byte_cnt_lut[6]     = 2
    byte_cnt_lut[7]     = 4
    byte_cnt_lut[8]     = 8

    local read_type_lut = {}
    read_type_lut[0]    = "b"
    read_type_lut[1]    = "h"
    read_type_lut[2]    = "i"
    read_type_lut[3]    = "b"
    read_type_lut[4]    = "H"
    read_type_lut[5]    = "I"
    read_type_lut[6]    = "NOPE"
    read_type_lut[7]    = "f"
    read_type_lut[8]    = "l"

    local type_string_lut = {}
    type_string_lut[0]  = "int8"    -- b
    type_string_lut[1]  = "int16"   -- h
    type_string_lut[2]  = "int32"   -- i
    type_string_lut[3]  = "int8"    -- b
    type_string_lut[4]  = "int16"   -- H
    type_string_lut[5]  = "int32"   -- I
    type_string_lut[6]  = "float16" -- NOPE
    type_string_lut[7]  = "float"   -- f
    type_string_lut[8]  = "double"  -- l


    function to_table( push_table, ... ) 

        for k,v in ipairs( {...} ) do
            if k > 1 then  
                --print("doing", k, v)
                table.insert( push_table,v)
            end
        end

        return push_table
    end

    function parseString( s_ix, length )
    	local substr = string.sub( data, s_ix, s_ix + length -1 )
        return substr
    end

    function parseValue( typ, offset, length, push_table )
        local get_bytes = 0
        local read_len  = length or 1


        if typ == "i" or typ == "I" or typ == "f" then
            get_bytes = 4
        elseif typ == "h" or typ == "H" then
            get_bytes = 2
        elseif typ == "c" or typ == "b" then
            get_bytes = 1
        end

        local val_string    = parseString( offset, get_bytes * read_len)

        offset = offset + get_bytes * read_len

        if read_len > 1 then 
            
            if not push_table then push_table = {} end

            return to_table( push_table, bunpack( val_string, typ .. length) ), offset
        else 
        	-- print("read len", read_len, string.len(val_string), offset, offset + get_bytes * read_len - 1)
            local wut,value = bunpack(val_string, typ)
            return value,offset
        end
    end

    -- trim string terminators
    function trim(s)
        local o = ""
        for i = 1, #s do
            local c = s:sub(i,i)
            local b = string.byte(c)
            if (b ~= 0) then
                o = o .. c
            end
        end
        return o
    end

    function binary_header( ) 
        local header = {}

        header.pxf_identifier   = parseString( 1, 4 )
        header.format_version   = parseValue( "f", 5 )
        header.entry_count      = parseValue( "I", 9 )

        -- io.write( "pxf_identifier : " .. tostring(header.pxf_identifier) 
        -- 	..  " format_version: " .. tostring(header.format_version) 
        -- 	.. ", entry_count: " .. tostring(header.entry_count) .. "\n")

        return header, 13
    end

    function buffer_header( offset )
        local header        = {}
        header.data_size    = parseValue( "I", offset )
        header.sub_buffer   = parseValue( "I", offset + 4 )
        header.external     = parseValue( "H", offset + 8 )
        header.name_size    = parseValue( "H", offset + 10 )
        header.name         = trim(parseString( offset + 12, header.name_size ))
        header.size         = 12 + header.name_size

        --io.write("buffer header size: " .. tostring(header.name_size) .. " raw: '" .. tostring(header.name) .. "'\n")

        return header, offset + header.size
    end

    function entry_header( offset )
        local header            = {}
        header.data_size        = parseValue( "I", offset )
        header.sub_buffers      = parseValue( "I", offset + 4 )
        header.cmp_size         = parseValue( "H", offset + 8 )
        header.elements         = parseValue( "H", offset + 10 )
        header.type             = parseValue( "H", offset + 12 )
        header.name_size        = parseValue( "H", offset + 14 )
        header.name             = trim(parseString( offset + 16, header.name_size ))
        header.size             = 16 + header.name_size

        return header,header.size + offset
    end

    function get_entry( offset )
        local entry, offset = entry_header( offset )
        local comp_byte_cnt     = byte_cnt_lut[entry.type]

        -- local entry             = {}
        entry.data              = {}
        entry.size              = entry.cmp_size
        --entry.type            = type_string_lut[entry.type]
        entry.format            = type_string_lut[entry.type]
        entry.num_components    = entry.data_size / comp_byte_cnt

        --print("offset:",offset)

        for sb=0,entry.sub_buffers-1,1 do
            --print("doing sub buffer " .. sb)
            local sub_buffer        = {}

            sub_buffer, offset      = buffer_header( offset )
            sub_buffer.read_size    = sub_buffer.data_size / comp_byte_cnt

            if sub_buffer.external > 0 then
                --print("No support for external sub buffers yet..")
            else
                sub_buffer.data         = {}
                sub_buffer.read_type    = read_type_lut[entry.type]  

                local data              = {}
                local chunksize         = 1024 * 4

                local num_chunks        = math.floor(sub_buffer.read_size / chunksize)
                --local chunks            = {}
                local bytes_left        = ((sub_buffer.read_size / chunksize) - num_chunks) * chunksize

                local step              = math.floor( sub_buffer.read_size / 100 ) 

                -------------------------
                -- unpack data from input 
                for i=1,num_chunks,1 do 

                    -- --print("Doing chunk",i,"bytes read", i * chunksize)

                    if ( i / num_chunks ) % 0.1 == 0 then 
                        -- print("Doing chunk " .. i .. ", bytes read: " .. i * chunksize)
                    end

                    data, offset        = parseValue( sub_buffer.read_type, offset, chunksize, entry.data ) --sub_buffer.read_size )

                    --table.insert( chunks, data )
                end

                local extra_data = {}
                extra_data, offset        = parseValue( sub_buffer.read_type, offset, bytes_left, entry.data ) --sub_buffer.read_size )               
            end
        end



        ----- WAAAAAAT THA FAAAAAAK::::>>>
        entry.type = entry.format
        entry.format = nil
        entry.elements = nil
        entry.cmp_size = nil
        entry.data_size = nil
        entry.num_components = nil
        entry.name_size = nil
        --entry.name = nil
        return entry, offset
    end

    local geometry          = {}
    local entries           = {}
    local header, offset    = binary_header()
    geometry.buffers        = {}


    -- return nil

    for e=1,header.entry_count,1 do
        local new_entry = nil

        new_entry,offset = get_entry( offset )
        geometry.buffers[ new_entry.name ] = new_entry
        geometry.buffers[ new_entry.name ].name = nil
    end
    
    return geometry
end


function load_external_geometry( json_path )
    if json_path:sub(-4) == ".bin" then 

        local f,e = io.open( json_path, "rb" )

        if ( e ) then error("load external geometry: " .. e) end

        local buffer_data = f:read("*a")

        f:close()

        local bin_geo = convert_binary_to_lua( buffer_data )
        buffer_data = nil
        collectgarbage()

        return bin_geo
    end

    local lua_path = json_path .. ".lua"
    if (json_path:sub(-4) ~= ".lua") then
        local f,e = io.open(lua_path, "r")
        if (not f) then
            -- convert geometry json to lua file
            convert_json2lua_file( json_path, lua_path, "return " )
        end
    else
        lua_path = json_path
    end
    ---------------------------------------------------------
    -- load Lua file
    local f,e = io.open(lua_path, "rb") --io.open(lua_path, "rb")
    if not f then
        error("File could not be opened!")
        return true, 0.0
    end

    -- concat all file chunks into a string
    local geometry_string = f:read("*a")--table.concat(chunks)
    f:close()

    --------------------------------
    -- load string as Lua
    local a,b = loadstring(geometry_string, lua_path)
    if (not a) then
        error(b)
        return true, 0.0
    end
    
    local geometry_obj = a()
    geometry_obj = { buffers = geometry_obj }
    return geometry_obj
end