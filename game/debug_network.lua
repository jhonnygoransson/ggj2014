local coolnet = require("game/coolnet")
local debug_network_screen = {}
local broadcast = nil
local serverlist = {}

debug_network_screen.init = function( self, game )
	
	self.type = arg[1]

	if (self.type == "client") then
		self.client, err = coolnet.create_client( arg[2], arg[3], function () end, function () print("connected") end, function () print("disconnected"); game:pop_screen() end )
		if (not self.client) then
			error("Could not connect to server: " .. tostring(err))
		end
		self.client:send("echo", "DUNDER OCH KNUKLAR")
		self.client:send("echo", "DUNDER OCH KNUKLAR")
		self.client:send("echo", "DUNDER OCH KNUKLAR")
		self.client:send("echo", "DUNDER OCH KNUKLAR")
		self.client:send("echo", "DUNDER OCH KNUKLAR")
		self.client:send("echo", "DUNDER OCH KNUKLAR")
		self.client:send("echo", "DUNDER OCH KNUKLAR")
		self.client:send("echo", "DUNDER OCH KNUKLAR")
		self.client:send("echo", "DUNDER OCH KNUKLAR")

	elseif (self.type == "server") then
		self.server = coolnet.create_server( arg[2], arg[3] )
	else
		error("Unknown network parameter!!!")
	end
	
end

local poop = nil

debug_network_screen.update = function( self, game, dt, vis )
	if (self.type == "client") then
		self.client:update()

		repeat
	        local ev = game:popevent()
	        if ev ~= nil then
	            print("debug client event", ev.code, ev.key, ev.action)
	            self.client:send("keyboard", {key = ev.key, action = ev.action})
	            if ev.action == "OFF" then
	            	if ev.key == "ESC" then
	            		game.running = false
	            	elseif (ev.key == "LEFT") then

	            		-- self.client:send_udp("echo", "asd")
	            	elseif (ev.key == "RIGHT") then
	            		-- for k,v in pairs(self.client.world_objects) do
	            		-- 	print(k,v)
	            		-- 	for kk,kv in pairs(v.vars) do
	            		-- 		print(kk,kv.value)
	            		-- 	end
	            		-- end
	            	end
	            end
	        end
	    until ev == nil

		-- i have a dream:
		-- local test = self.client:new_world_object( "class_name" )

	elseif (self.type == "server") then
		self.server:update()

		repeat
	        local ev = game:popevent()
	        if ev ~= nil then
	            print("debug server event", ev.code, ev.key, ev.action)
	            if ev.action == "OFF" then
	            	if ev.key == "ESC" then
	            		game.running = false
	            	elseif (ev.key == "LEFT") then
	            		poop = self.server:create_object( "test" )
	            		poop:add_field( "y", 100.0, "linear" )
	            		-- self.server:send_to_all_udp("echo", "hi from server")
	            	elseif (ev.key == "RIGHT") then
	            		print(poop.vars)
	            		poop:set_field( "y", poop.vars.y.value + 1 )
	            	end
	            end
	        end
	    until ev == nil

	end

end

debug_network_screen.draw = function( self, game, vis )

	gamefont.draw( game.settings.win_w / 2 - 200, game.settings.win_h / 2 - 24, 16, "debug networking: " .. self.type, nil, vis)

end

return debug_network_screen