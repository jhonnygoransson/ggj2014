local camera = {
	dirty = true,
	projection = mat4.ortho(0, 1, 0, 1, -1, 1),
	unit = 70,
	scale = 1.0,
	x = 0,
	y = 0
}

function camera:set_gameobject(game)
	self.game = game
	self.dirty = true
end

function camera:look_at(x, y)
	self.x = x
	self.y = y

	self.dirty = true
end

function camera:translate(dx, dy)
	self:look_at(self.x + dx, self.y + dy)
end

function camera:lock_to(object)
	self.locked = object
end

function camera:update(dt)
	local x, y
	if self.locked then
		-- Try to accept a few different methods of representing object position
		if self.locked.x and self.locked.y then
			x = self.locked.x
			y = self.locked.y
		elseif self.locked.getPosition then
			x, y = self.locked:getPosition()

			if type(y) ~= "number" then
				y = x.y
				x = x.x
			end
		end

		if x and y and (self.x ~= x or self.y ~= y) then
			self.x = x
			self.y = y
			self.dirty = true

		end
	end
end

function camera:zoom(scale)
	if self.scale ~= scale then
		self.scale = scale
		self.dirty = true
	end
end

function camera:get_projection()
	if self.dirty then
		local w = (self.game.settings.win_w / self.unit) * self.scale
		local h = (self.game.settings.win_h / self.unit) * self.scale

		self.projection = mat4.ortho(
			self.x - w/2, self.x + w/2,
			self.y + h/2, self.y - h/2,
			-1, 1)

		self.dirty = false
	end

	return self.projection
end

return camera
