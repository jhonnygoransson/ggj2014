attribute vec3 position;

uniform mat4 pmtx;
uniform mat4 mmtx;

void main()
{
	gl_Position = pmtx * vec4(position.xyz, 1.0);
}
