CheckVersion("0.4")

local settings = NewSettings()
settings.optimize = 1

if family == "unix" then
	if platform == "macosx" then
		settings.dll.libpath:Add("/Users/work/Documents/luajit-2.0/src/")
		settings.dll.libpath:Add("../../Downloads/Box2D_v2.2.1/Build/Box2D/")
		settings.dll.libs:Add("Box2D")
		settings.dll.libs:Add("luajit-5.1.2.0.2")
	elseif platform == "linux" then
		settings.cc.flags:Add("-fPIC")
		settings.dll.libs:Add("Box2D")
		settings.cc.includes:Add("/usr/include/luajit-2.0/")
	end
elseif family == "windows" then

end

settings.cc.includes:Add("src")
settings.cc.includes:Add("../../Downloads/Box2D_v2.2.1/")
settings_src = { CollectRecursive("src/*.cpp") }
settings_objs = Compile(settings, settings_src)

lib = SharedLibrary(settings, "luabox2d", settings_objs)
