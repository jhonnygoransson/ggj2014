local map = {}

function map:init(path)
	self.map     	= require(path)
	self.physics 	= require("game/physics")
	self.tileshader = pxf.shader("data/shaders/tile.vs", "data/shaders/tile.fs", true)

	-- Calculate uv coordinates for tile index in tileset
	function get_uv(tile_idx, tileset)
		-- Tile index starts at 1, so offset now to avoid fiddling with 1's
		tile_idx = tile_idx - 1

		local n_tiles_w = math.floor(
			tileset.imagewidth / (tileset.tilewidth + tileset.spacing))
		local n_tiles_h = math.floor(
			tileset.imageheight / (tileset.tileheight + tileset.spacing))

		assert(n_tiles_w * n_tiles_h > tile_idx and tile_idx >= 0,
			   "tile_idx out of bounds: " .. tile_idx)

		local imagewidth = n_tiles_w * (tileset.tilewidth + tileset.spacing)
		local imageheight = n_tiles_h * (tileset.tileheight + tileset.spacing)

		local idx_x = tile_idx % n_tiles_w
		local idx_y = math.floor(tile_idx / n_tiles_w)

		assert(idx_x < n_tiles_w, "tile x offset can't be "..idx_x.."!")
		assert(idx_y < n_tiles_h, "tile y offset can't be "..idx_y.."!")

		local left_px = (idx_x * (tileset.tilewidth + tileset.spacing))
		local top_px = (idx_y * (tileset.tileheight + tileset.spacing))

		return {
			{
				left_px / tileset.imagewidth,  -- s0
				top_px / tileset.imageheight -- t0
			},

			{
				(left_px + tileset.tilewidth) / tileset.imagewidth, -- s1
				top_px / tileset.imageheight, -- t1
			},

			{
				(left_px + tileset.tilewidth) / tileset.imagewidth, -- s2
				(top_px + tileset.tileheight) / tileset.imageheight, -- t2
			},

			{
				left_px / tileset.imagewidth, -- s3
				(top_px + tileset.tileheight) / tileset.imageheight -- t3
			}
		}
	end

	-- Add a pxf.quadbatch to a map layer, using tile set
	function build_layer(map_layer, tile_set)
		local q = pxf.quadbatch()
		q:begin()

		for i=1,#map_layer.data do
			tile_idx = map_layer.data[i]
			if tile_idx ~= 0 then
				q:setcoords(get_uv(tile_idx, tile_set))

				local x = (i - 1) % map_layer.width -- +1, since index starts at 1
				local y = math.floor((i-1) / map_layer.width) -- dito

				self.physics.new_platform(x, y, 1, 1)
				q:addcentered(x+0.5, y+0.5, 1, 1)
			end
		end

		q:finish()

		map_layer.quadbatch = q
	end

	for i=1,#self.map.layers do
		build_layer(self.map.layers[i], self.map.tilesets[1])
	end

	return self
end

function map:set_tiletexture(tm)
	self.tiletexture = tm
end

function map:update()
end

function map:draw(camera)
	if( not self.tiletexture ) then return end
	self.tileshader:bind()
	self.tileshader:setuniform("mat4", "p_matrix", camera:get_projection())
	self.tileshader:setuniform("i","tex0",0)
	self.tiletexture:bind(0)

	for i=1,#self.map.layers do
		self.map.layers[i].quadbatch:draw(self.tileshader,
			{position = true, uv0 = true, normal = false})
	end

	self.tiletexture:unbind()
	self.tileshader:unbind()
end

return map
