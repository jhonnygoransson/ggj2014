local socket = require("socket")

local coolnet = {}
coolnet.broadcast_port = 463200

function serialize (tt, indent, escapechar)

	indent = indent or ""
	escapechar = escapechar or [["]]
	if ("table" == type(tt)) then
		local o = { }
		local plain_list = true

		for k,v in pairs(tt) do
			if ("number" ~= type(k)) then
				plain_list = false
				break
			end
		end

		if (plain_list) then
			for k,v in pairs(tt) do
				table.insert(o, indent .. "  " .. serialize(v, indent .. "  ", escapechar))
			end
		else
			for k,v in pairs(tt) do
				table.insert(o, indent .. "  [" .. escapechar .. k .. escapechar .. '] = ' .. serialize(v, indent .. "  ", escapechar))
			end
		end
		return ("{" .. table.concat(o, ",") .. "" .. indent .. "}")

	elseif ("number" == type(tt)) then
		return (tostring(tt))

	elseif ("string" == type(tt)) then
		return ('[[' .. tt .. ']]')

	elseif ("function" == type(tt)) then
		return ('nil')
	else
		return (tostring(tt))
	end
	
end

function parse_message( msg )

	if (msg == nil) then return false end

	local a,b = loadstring("return " .. tostring(msg))
    if (not a) then

    	-- local f,e = io.open("failed_msgs.txt", "w+") 
    	-- f:write("\n\nNew failed - " .. os.date() .. "\n\n" )
    	-- f:write(msg)
    	-- f:close()

    	print("errorstring for message",msg,"error: " .. tostring(b))
    	error("parse_message error parsing string")

    	return false, b
    end

    local res = a()

	return true, res
end

function datagrams_from_buffer( buf )
	
	local new_buf = buf
	local packets = {}

	while (true) do
		-- print("new_buf len: " .. tostring(string.len(new_buf)))
		local size_marker = string.sub(new_buf, 1, 5)

		if (string.len(size_marker) < 4) then
			-- print("size_marker too short!")
			break
		end
		local junk, size = bunpack( size_marker, "I" )
		-- print(size)

		local packet = string.sub(new_buf, junk, size+junk-1)
		if (string.len(packet) < size) then
			print("too short: ", string.len(packet))
			break
		end

		-- print(packet)
		table.insert(packets, packet)

		-- remove header+datalen from buf
		new_buf = string.sub(new_buf, 5 + size)
	end

	-- print("#packets: " .. tostring(#packets))
	return new_buf, packets

end

function unsafe_send( sock, data )
	-- print(data)
	local len = string.len(data)
	local full_packet = bpack( "I", len ) .. data
	sock:send(full_packet)

end

function safe_send( sock, data )
	
	local len = string.len(data)
	sock:send(tostring(len) .. "\n")
	sock:send(data)

end

function safe_recv( sock )

	--------
	-- TODO check for "timeout" errors below and read partial instead

	local got_len = false
	local len_str = ""
	while (not got_len) do
		local _str, err, _partial = sock:receive('*l')
		if (_str == nil) then
			-- print( "socket receive err:", err, _partial )
			-- os.exit(-1)
			-- if (err == "closed") then
			return nil, err
			-- end
		end
		len_str = len_str .. _str
		got_len = true
	end
	local len_number = tonumber(len_str)
	-- print(len_number)

	local full_data = ""
	local got_all_data = false
	while (not got_all_data) do
		local _str, err, _partial = sock:receive(len_number)
		if (_str == nil) then
			print( "socket receive err:", err, _partial )
			os.exit(-1)
		end
		full_data = full_data .. _str
		got_all_data = true
	end

	-- print(full_data)
	return full_data
	
end

coolnet.create_server = function ( host, port, event_cb, connect_cb, disconnect_cb )
	
	local server = {}

	server.broadcast = socket.udp()
	server.broadcast:setoption("broadcast", true)
	server.master = socket.tcp()
	server.client_objects = {}
	server.client_objects_udp = {}
	server.client_socks   = { server.broadcast, server.master}
	server.synced_world_objects = {}
	server.dirty_fields = {}
	server.total_obj_count = 0
	-- server.event_cb = event_cb

	local s, sockerr = server.broadcast:setsockname( "*", coolnet.broadcast_port )
	if not s then 
		error("Manager: Discovery socket error: " .. tostring(sockerr))
	end

	server.clean_up = function ( self )
		for k,v in pairs(self.client_socks) do
			v:close()
		end
	end

	server.accept_new_client = function ( self, client_sock  )
		
		print("New client connected!", client_sock)
		self.client_objects[client_sock] = {
			server  = self,
			tcpsock = client_sock,
			udpsock = socket.udp(),
			udpvalid = false,
			udp_buff = "",
			serv = self,
			clean_up = function ( self )
				
				self.server.client_objects[self.tcpsock] = nil
				self.server.client_objects_udp[self.udpsock] = nil

				for k,v in pairs(self.server.client_socks) do
					if (v == self.tcpsock) then
						self.server.client_socks[k] = nil
					end

					if (v == self.udpsock) then
						self.server.client_socks[k] = nil
					end
				end

			end,
			send = function ( self, msg_type, msg )
				-- safe_send( self.tcpsock, msg_type, msg )
				-- print("Trying to send ", msg_type)
				safe_send( self.tcpsock, serialize( { type = msg_type, payload = msg } ) )
			end,
			send_udp = function ( self, msg_type, msg )
				unsafe_send( self.udpsock, serialize( { type = msg_type, payload = msg } ) )
			end,
			recv = function ( self )
				local data, err = safe_recv( self.tcpsock )
				if (data == nil) then
					if (err == "closed") then
						disconnect_cb( self )
						self:clean_up()
					else
						-- print("error receive", err)
					end
					return
				end

				local succ, data = parse_message(data)
				if (not succ) then
					error("parse_message error: " .. tostring(data))
				end

				if (data.type == "echo") then
					print("TCP echo:", self.tcpsock, "->", data.payload)
				else
					-- print("Unknown data type: " .. tostring(data.type))
					event_cb( self, data.type, data.payload )
				end

			end,
			recv_udp = function ( self )
				local ip, port

				while (true) do
					-- print("udp recv")
					if self.udpvalid then
						ret = self.udpsock:receive()
					else
						ret, _ip, _port = self.udpsock:receivefrom()
					end
					if (not ret) then

						if (_ip == "closed") then
							disconnect_cb( self )
							self:clean_up()
						else
							-- print("error receive", _ip, self.udp_buff)
						end
						break

					end
					-- print("got", ret)
					self.udp_buff = self.udp_buff .. ret

					ip = _ip
					port = _port
				end

				self.udp_buff, packets = datagrams_from_buffer( self.udp_buff )
				
				for k,v in pairs(packets) do
					local succ, data = parse_message(v)
					if (not succ) then
						error("parse_message error: " .. tostring(data))
					end

					if (data.type == "echo") then
						print("UDP echo:", self.udpsock, "->", data.payload)
					elseif (data.type == "ping") then
						print("UDP ping:", self.udpsock, "->", data.payload, ip, port)
						self.udpvalid = true
						print(self.udpsock:setpeername( ip, port )) --- <--
						print(self.udpsock)

					else
						event_cb( self, data.type, data.payload )
					end
					
				end

			end

		}
		table.insert( self.client_socks, client_sock )

		-- grab any udp-port we can get
		local udpsock = self.client_objects[client_sock].udpsock
		udpsock:settimeout( 0 )
		udpsock:setsockname("*", 0)
		local udp_ip, udp_port = udpsock:getsockname()
		self.client_objects[client_sock].udp_port = udp_port
		table.insert( self.client_socks, udpsock )
		self.client_objects_udp[ udpsock ] = self.client_objects[client_sock]

		-- notify client that we have an open udp-port for hen
		self.client_objects[client_sock]:send( "echo", "Welcome!" )
		self.client_objects[client_sock]:send( "udpinfo", { ip = udp_ip, port = udp_port } )

		self:send_full_state(self.client_objects[client_sock])

		connect_cb(self.client_objects[client_sock])

	end
	server.send_to_all = function ( self, msg_type, msg )
		
		for k,v in pairs(self.client_objects) do
			v:send( msg_type, msg )
		end

	end
	server.send_to_all_udp = function ( self, msg_type, msg )
		
		for k,v in pairs(self.client_objects) do
			if (v.udpvalid) then
				v:send_udp( msg_type, msg )
			end
		end

	end
	server.update = function ( self )
		-- select on listener-socket and client sockets
		local recvt, sendt, err = socket.select(self.client_socks, {}, 0 )

		-- time to accept a new client
		if (recvt[self.master]) then
			self:accept_new_client( self.master:accept() )
			recvt[self.master] = nil
		end

		if ( recvt[self.broadcast] ) then
			local msg, taddr, tport = self.broadcast:receivefrom()

			if msg == "PXF_HELLO" then
				print("hello from ", taddr, tport,self.master_host, self.master_port)
				self.broadcast:sendto( serialize( { type = "SERVER_HELLO", payload = { ip = self.master_host, port = self.master_port } } ), taddr, tport )
			end
		end

		-- loop through every 
		for k,v in pairs(recvt) do
			if (type(k) ~= "number") then
				-- print("receive", k, v)
				-- print(k:receive('*l'))
				if (self.client_objects[k]) then
					self.client_objects[k]:recv()
				elseif (self.client_objects_udp[k]) then
					self.client_objects_udp[k]:recv_udp()
				end
			end
		end

		-- sync the world with all clients
		for k,v in pairs(self.dirty_fields) do
			self:send_to_all_udp( "update", v )
		end

		self.dirty_fields = {}
	end

	server.send_full_state = function ( self, client )
		
		for cname,c in pairs(self.synced_world_objects) do
			for _,obj in pairs(c) do
				client:send( "spawn", { class = cname, object = obj })
			end
		end

	end

	-- serverside synced object creation
	server.create_object = function ( self, class )

		print("Creating a new object of class " .. tostring(class))
		local server_self = self
		self.total_obj_count = self.total_obj_count + 1

		local synced_object = {
			id              = self.total_obj_count,
			-- server_instance = self,
			class           = class,
			vars            = {}
		}

		-- add a synced field to object
		synced_object.add_field = function ( self, name, value, prediction )
			self.vars[name] = {
				object_id  = self.id,
				name       = name,
				value      = value,
				prediction = prediction,
				dirty      = true
			}
			table.insert( server_self.dirty_fields, self.vars[name] )
			-- TODO notify clients of this field, or not
		end

		-- update a field
		synced_object.set_field = function ( self, name, value )
			self.vars[name].value = value
			self.vars[name].dirty = true
			table.insert( server_self.dirty_fields, self.vars[name] )
		end

		-- local 
		-- synced_object.remove = function ( self )
			
		-- end

		if (not self.synced_world_objects[class]) then
			self.synced_world_objects[class] = {synced_object}
		else
			table.insert( self.synced_world_objects[class], synced_object )
		end

		self:send_to_all( "spawn", { class = class, object = synced_object })

		return synced_object
	end

	-- open server listener socket
	server.master:setoption('reuseaddr', true)
	server.master:bind( host, port )
	server.master:listen()
	server.master_host = host
	server.master_port = port

	return server
end

-----------------------------------------
-- create a network game client instance,
-- connects to a server isntance
coolnet.create_client = function ( host, port, event_cb, connect_cb, disconnect_cb )

	local client = {}
	client.tcpsock = socket.tcp()
	client.udpsock = socket.udp()
	-- client.event_cb = event_cb
	client.udp_buff = ""

	client.clean_up = function ( self )
		self.tcpsock:close()
		self.udpsock:close()
	end

	client.update = function ( self )
		
		-- select on server tcp and udp sockets
		local recvt, sendt, err = socket.select( { self.tcpsock, self.udpsock }, {}, 0 )

		if (recvt[self.tcpsock]) then
			-- print("receive", k, v)
			-- print(k:receive('*l'))
			self:recv()
		end


		if (recvt[self.udpsock]) then
			-- print("receive", k, v)
			-- print(k:receive('*l'))
			self:recv_udp()
		end

	end

	client.send_udp = function ( self, msg_type, msg )
		unsafe_send(self.udpsock, serialize( { type = msg_type, payload = msg } ) )
	end

	client.send = function ( self, msg_type, msg )
		safe_send(self.tcpsock, serialize( { type = msg_type, payload = msg } ) )
	end

	client.recv = function ( self )
		
		local data, err = safe_recv( self.tcpsock )

		if (data == nil) then
			if (err == "closed") then
				disconnect_cb( self )
			else
				print("error receive", err)
			end
			return
		end

		local succ, data = parse_message(data)
		if (not succ) then
			error("parse_message error: " .. tostring(data))
		end

		if (data.type == "echo") then
			print("TCP echo:", self.tcpsock, "->", data.payload)
		elseif (data.type == "udpinfo") then

			-- new udp information, remember these settings
			print(data.payload.ip, data.payload.port)
			print(self.udpsock:setpeername(--[[data.payload.ip]] host, data.payload.port))
			self.udpsock:settimeout( 0 )
			print(self.udpsock)

			self:send_udp( "ping", "hi bob")

		elseif (data.type == "spawn") then
			print("Spawning a " .. tostring(data.payload.class) .. " on client.")
			local obj = data.payload.object

			-- get a field
			obj.get_field = function ( self, name )
				if (self.vars[name]) then
					return self.vars[name].value
				end

				return nil
			end

			if (self.world_classes[data.payload.class]) then
				table.insert(self.world_classes[data.payload.class], obj )
			else
				self.world_classes[data.payload.class] = { obj }

			end
			self.world_objects[obj.id] = obj

		else
			--print("Unknown TCP data type: " .. tostring(data.type))
			event_cb( data.type, data.payload )
		end

	end

	client.recv_udp = function ( self )
				
		while (true) do
			-- print("udp recv")
			local ret, err = self.udpsock:receive()
			if (not ret) then
				if (err == "closed") then
					disconnect_cb( self )
				else
					-- print("error receive", err)
				end
				break
			end
			-- print("got", ret)
			self.udp_buff = self.udp_buff .. ret
		end

		-- print(self.udp_buff)
		self.udp_buff, packets = datagrams_from_buffer( self.udp_buff )
		
		for k,v in pairs(packets) do
			local succ, data = parse_message(v)
			if (not succ) then
				error("parse_message error: " .. tostring(data))
			end

			if (data.type == "echo") then
				print("UDP echo:", self.udpsock, "->", data.payload)
			elseif (data.type == "update") then
				-- print("UDP update:", self.udpsock, "->", data.payload)

				--[[
					self.vars[name] = {
						object_id  = id,
						name       = name,
						value      = value,
						prediction = prediction,
						dirty      = true
					}
				]]
				-- print("Updating a field: ", data.payload.name)
				if (not client.world_objects[data.payload.object_id]) then
					print("Got update for unknown object: ", data.payload.object_id, "field:", data.payload.name)
				else
					if (client.world_objects[data.payload.object_id].vars[data.payload.name]) then

						client.world_objects[data.payload.object_id].vars[data.payload.name].value      = data.payload.value
						client.world_objects[data.payload.object_id].vars[data.payload.name].prediction = data.payload.prediction

					else
						client.world_objects[data.payload.object_id].vars[data.payload.name] = data.payload
						
					end
				end

			else
				print("UDP event: " .. tostring(data.type))
				event_cb( data.type, data.payload )
			end
			
		end

	end

	client.world_classes = {} -- indexed by class
	client.world_objects = {} -- indexed by id

	-- try to connect to server!
	local ret, err = client.tcpsock:connect( host, port )
	if (ret) then
		connect_cb()
		client:send("echo", "Dad?...")
		return client
	else
		disconnect_cb()
		return nil, err
	end

end



return coolnet
