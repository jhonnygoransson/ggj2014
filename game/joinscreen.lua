local ffi  = require( "ffi" )
local glfw = require( "ffi/glfw3" )
local gl   = require( "ffi/OpenGL" ) 
local pxf   = require( "pxf/pxf" ) 
local coolnet = require("game/coolnet")

require("game/text")

local join_menu = {}
-- local broadcast = nil
local serverlist = {}

join_menu.refresh = function(self)
    serverlist = {}
    join_menu.options = deepcopy(join_menu.options_default)
    print("refreshing list")
	broadcast:sendto( "PXF_HELLO", "255.255.255.255", coolnet.broadcast_port )
end

join_menu.addserver = function(self, ip, port )

	local labelstr = ip .. ":" .. port

	local found = false

	for k,v in pairs(self.options) do 
		if v.label == labelstr then found = true
		end
	end

	if not found then

		for k,v in pairs(arg) do print("HEJ", k,v) end 

		local optiontbl = {}
		optiontbl.label = labelstr
		optiontbl.screen = "game/gamescreen"

		-- arg[1] = "client"
		-- arg[2] = tostring(ip)
		-- arg[3] = tostring(port)

		optiontbl.knode = "arg[1] = '" .. ip .. "'; arg[2] = '" .. port .. "';"
		table.insert( self.options, optiontbl )
	end

end

join_menu.init = function ( self, game )
    self.game = game

    if (not broadcast) then
        broadcast = socket.udp()
    	broadcast:setoption("broadcast",true)
    	
    	local s, sockerr = broadcast:setsockname( "*", coolnet.broadcast_port + 1)

    	if not s then 
    		error("Client: Discovery socket error: " .. tostring(sockerr))
    	end	
    end

    join_menu:refresh()
end

join_menu.update = function ( self, game, dt, visibility )
	
	local recvt, sendt, err = socket.select( { broadcast }, {}, 0 )
	if recvt[broadcast] then
		local msg, taddr, tport = broadcast:receivefrom()

		local succ, msg = parse_message( msg )

		if succ then
			if msg.type == "SERVER_HELLO" then
				local payload = msg.payload

				local server_ip   = payload.ip
				local server_port = payload.port

				local idstring = server_ip .. ":" .. server_port

				serverlist[idstring] = { ip = server_ip, port = server_port }

				join_menu:addserver( server_ip, server_port )
			end
		else
			error("not succ")
		end
	end


    if (visibility < 1) then
        return
    end

    local gametype = nil
    repeat
        local ev = game:popevent()
        if ev ~= nil then
            print("main menu event", ev.code, ev.key, ev.action)
            if ev.action == "OFF" then
                if ev.key == "ESC" then
                    game:pop_screen()
                    print("popping mainmenu screen")
                -- tempmenysaker
                elseif ev.key == "F5" then
                    join_menu:refresh()
                -- tempmenysaker
                elseif ev.key == "LEFT" then
                    --print "Starta snigelspel"
                    --gametype = "snigel"
                elseif ev.key == "UP" then
                    self.current_option = self.current_option - 1
                    if (self.current_option < 1) then
                        self.current_option = #self.options
                    end
                    --print "Starta multispel"
                    --gametype = "multi"
                elseif ev.key == "DOWN" then
                    self.current_option = self.current_option + 1
                    if (self.current_option > #self.options) then
                        self.current_option = 1
                    end
                    --print "Bygg banana"
                    --gametype = "bananbygge"
                elseif ev.key == "ENTER" then

                    print("selecting: " .. self.options[self.current_option].label)

                    if (self.options[self.current_option].knode) then
                        local a = loadstring(self.options[self.current_option].knode)
                        a = setfenv(a, { game = game, arg = arg })
                        a()
                    end

                    if (self.options[self.current_option].screen) then

                        -- make sure old screen is removed
                        if (package.loaded[self.options[self.current_option].screen]) then
                            package.loaded[self.options[self.current_option].screen] = nil
                        end
                        game:push_screen( require(self.options[self.current_option].screen) )

                    end
                end
            end
        end
    until ev == nil
end

join_menu.options_default = {
    {
        label = "Back",
        knode = "game:pop_screen()"
    },
}


join_menu.current_option = 1
join_menu.draw = function ( self, game, vis )
    -- gl.glMatrixMode(gl.GL_PROJECTION);
    -- gl.glLoadIdentity();
    -- gl.glOrtho(0, game.settings.win_w, game.settings.win_h, 0, 0, 1)

    -- gl.glMatrixMode( gl.GL_MODELVIEW );
    -- gl.glLoadIdentity();

    local y = 24
    for k,v in pairs(self.options) do
        local extra_label = "  "
        if (self.current_option == k) then
            extra_label = "> "
        end
        -- draw_string( 24, y, extra_label .. v.label)
        gamefont.draw( 24, y, 16, extra_label .. v.label, nil, vis)
        y = y + 14
    end

    draw_flush()
end

return join_menu