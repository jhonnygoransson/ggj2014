local ffi  = require( "ffi" )
local glfw = require( "ffi/glfw3" )
local gl   = require( "ffi/OpenGL" )
--local net  = require( "game/net" )
local socket = require "lib/socket"
--local environment = require "game/environment"

game = {}

local window_is_open = false
local function on_window_close(window)
	window_is_open = true
end

--------------------------------------------------------------------------------
-- screen methods
-- splash, menu and gameplay etc
local empty_screen = { init = function() end, update = function() end, draw = function() end }
game.screens = {empty_screen}
game.screen_change = 0 -- can be -1, 0 or 1
game.screen_speed_forward  = 1.3
game.screen_speed_backward  = 1.3
game.push_screen = function ( self, screen )
	table.insert(self.screens, screen )
	screen:init( self )
	self.screen_change = 1.0
end
game.pop_screen = function ( self )
-- print("popping")
	if (self.screen_change > 0) then
		self.screen_change = -(1.0 - self.screen_change)
	else
		self.screen_change = -1.0
	end
end

--------------------------------------------------------------------------------
-- Key callback queue

-- translate raw events to game events
game.translate_event = function(self, src)
	local tbl = {}
	tbl[glfw.GLFW_KEY_ESCAPE] = "ESC"
	tbl[glfw.GLFW_KEY_SPACE] = "SPACE"
	tbl[glfw.GLFW_KEY_ENTER] = "ENTER"
	tbl[glfw.GLFW_KEY_0] = "0"
	tbl[glfw.GLFW_KEY_9] = "9"
	tbl[glfw.GLFW_KEY_8] = "8"
	tbl[glfw.GLFW_KEY_LEFT]  = "LEFT"
	tbl[glfw.GLFW_KEY_UP]    = "UP"
	tbl[glfw.GLFW_KEY_DOWN]  = "DOWN"
	tbl[glfw.GLFW_KEY_RIGHT] = "RIGHT"
	tbl[glfw.GLFW_KEY_1] = "PL1_MIC1"
	tbl[glfw.GLFW_KEY_2] = "PL1_MIC2"
	tbl[glfw.GLFW_KEY_3] = "PL1_MIC3"
	tbl[glfw.GLFW_KEY_4] = "PL1_MIC4"
	tbl[glfw.GLFW_KEY_F5] = "F5"

	tbl["PLAYER1_MIC_1"] = "PL1_MIC1"
	tbl["PLAYER1_MIC_2"] = "PL1_MIC2"
	tbl["PLAYER1_MIC_3"] = "PL1_MIC3"
	tbl["PLAYER1_MIC_4"] = "PL1_MIC4"
	tbl["PLAYER2_MIC_1"] = "PL2_MIC1"
	tbl["PLAYER2_MIC_2"] = "PL2_MIC2"
	tbl["PLAYER2_MIC_3"] = "PL2_MIC3"
	tbl["PLAYER2_MIC_4"] = "PL2_MIC4"

	tbl["FLUTE1_SENS1"] = "1" -- osv?
	return tbl[src]
end

game._keyqueue = {}
game.popevent = function (self) return table.remove(self._keyqueue, 1) end
local function on_key_callback_newglfw(window, key, scancode, action, modifiers)
	if action == 1 then
		action = "ON"
	elseif action == 0 then
		action = "OFF"
	else
		return
	end
	local ev = {code = key, key = game:translate_event(key), action = action}
	table.insert(game._keyqueue, ev)
end

local function on_key_callback_oldglfw(window, key, action)
	if action == 1 then action = "ON" else action = "OFF" end
	local ev = {code = key, key = game:translate_event(key), action = action}
	table.insert(game._keyqueue, ev)
end

-------------------------------------------------------------------------------
-- spotify helpers
function play_uri(uri)
	libsp.play(false)
	local track = libsp.track_from_uri(uri)
	--local socket = require "lib/socket"
	while tostring(track) == "" do
		socket.select(nil, nil, 0.1)
	end
	libsp.load(track)
	libsp.play(true)
end

--------------------------------------------------------------------------------
-- init window and stuuuuuuff
local default_settings = {
		win_w = 512,
		win_h = 512,
		fullscreen = false
	}

--jit.off(game.init)
game.init = function ( self, settings )
	game.start_time = glfw.glfwGetTime()
	game.uptime = 0
	self.settings = settings
	
	assert( glfw.glfwInit() )

	
	if ffi.os == "Windows" then
        local desktop_mode = ffi.new( "GLFWvidmode[1]" )
        glfw.glfwGetDesktopMode( desktop_mode )
        local desktop_width, desktop_height = desktop_mode[0].width, desktop_mode[0].height
        local window_mode = glfw.GLFW_WINDOWED
        if (settings.fullscreen) then
                window_mode = glfw.GLFW_FULLSCREEN
                settings.win_w = desktop_width
                settings.win_h = desktop_height
        else
                local window_x = ( desktop_width - settings.win_w ) / 2
                local window_y = ( desktop_height - settings.win_h ) / 2
                glfw.glfwWindowHint( glfw.GLFW_POSITION_X, window_x )
                glfw.glfwWindowHint( glfw.GLFW_POSITION_Y, window_y )
        end
        glfw.glfwWindowHint(glfw.GLFW_WINDOW_NO_RESIZE, 1)
        self.window = glfw.glfwCreateWindow( settings.win_w, settings.win_h, window_mode, "Panflute Hero 2", nil )
        assert( self.window )
		glfw.glfwSetKeyCallback(self.window, on_key_callback_oldglfw)
	else
		local monitors = ffi.new( "GLFWmonitor*[1]" )
		local monitor_count = ffi.new("int[1]")
		monitors = glfw.glfwGetMonitors(monitor_count)
		local default_monitor = monitors[0]
		local fullscreen_monitor = nil
		
		local desktop_mode = ffi.new( "GLFWvidmode[1]" )
		desktop_mode = glfw.glfwGetVideoMode(default_monitor);
		local desktop_width, desktop_height = desktop_mode.width, desktop_mode.height

		if (settings.fullscreen) then
			settings.win_w = desktop_width
			settings.win_h = desktop_height
			fullscreen_monitor = default_monitor
		else
			self.window_x = ( desktop_width - settings.win_w ) / 2
			self.window_y = ( desktop_height - settings.win_h ) / 2
		end

		self.window = glfw.glfwCreateWindow( settings.win_w, settings.win_h, "Panflute Hero 2", fullscreen_monitor, nil )
		assert( self.window )
		if (not settings.fullscreen) then
			glfw.glfwSetWindowPos(self.window, self.window_x, self.window_y);
		end
		glfw.glfwSetWindowCloseCallback(self.window, on_window_close)
		glfw.glfwSetKeyCallback(self.window, on_key_callback_newglfw)
	end
	glfw.glfwMakeContextCurrent( self.window )
	glfw.glfwSwapInterval( 1/60 )

end

--------------------------------------------------------------------------------
-- main entrypoint
game.run = function ( self )

	--self:init(default_settings)

	local last_update = socket.gettime()	
	self.running = true
	while (self.running and not window_is_open) do

		local cur_update = socket.gettime()

		gl.glViewport( 0, 0, self.settings.win_w, self.settings.win_h )
		gl.glClearColor( 0, 0, 0, 1)
		gl.glClear( gl.GL_COLOR_BUFFER_BIT )
		gl.glClear( gl.GL_DEPTH_BUFFER_BIT )

		self:update(cur_update - last_update)
		self:draw()
		
		glfw.glfwSwapBuffers( self.window )


		glfw.glfwPollEvents()
		
		--print(game.screen_change)

		last_update = cur_update
	end

	glfw.glfwTerminate()

end

--------------------------------------------------------------------------------
-- updates the relative screens with delta in milliseconds
game.update = function ( self, dt )
	game.uptime = glfw.glfwGetTime() - game.start_time

	-- update screen change
	if (self.screen_change > 0) then
		self.screen_change = self.screen_change - self.screen_speed_forward * dt

		if (self.screen_change <= 0) then
			self.screen_change = 0
		end
	elseif (self.screen_change < 0) then
		self.screen_change = self.screen_change + self.screen_speed_backward * dt

		if (self.screen_change >= 0) then
			self.screen_change = 0

			-- remove top of screens stack
			table.remove(self.screens)

			if (#self.screens == 1) then -- only empty screen left -> quit
				self.running = false
			end
		end

	end

	--[[if (self.screen_change < 0) then
		self.screens[#self.screens - 1]:update( self, dt )
	end]]

	if (self.screen_change < 0) then
		self.screens[#self.screens - 1]:update( self, dt, 1.0 + self.screen_change )
		self.screens[#self.screens]:update( self, dt, -self.screen_change )
	elseif (self.screen_change > 0) then
		self.screens[#self.screens - 1]:update( self, dt, self.screen_change )
		self.screens[#self.screens]:update( self, dt, 1.0 - self.screen_change )
	else
		self.screens[#self.screens]:update( self, dt, 1.0 )
	end
end

--------------------------------------------------------------------------------
-- draws the screens that are currently visible
game.draw = function ( self )
	
	if (self.screen_change < 0) then
		self.screens[#self.screens - 1]:draw( self, 1.0 + self.screen_change )
		self.screens[#self.screens]:draw( self, -self.screen_change )
	elseif (self.screen_change > 0) then
		self.screens[#self.screens - 1]:draw( self, self.screen_change )
		self.screens[#self.screens]:draw( self, 1.0 - self.screen_change )
	else
		self.screens[#self.screens]:draw( self, 1.0 )
	end

	
end




return game
