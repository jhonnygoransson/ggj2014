local ffi  = require( "ffi" )
local gl   = require( "ffi/OpenGL" ) 

spritemanager = nil
spritemanager_generator = function()
	if ( spritemanager ~= nil ) then return spritemanager end

	print("Spritemanager: Creating new manager")

	local tilesperrow  	= 256 
	local s 			= {}
	s.sprites 			= {}

	function s:new(sprite_texture,size)
		print("Spritemanager: Adding new sprite sheet @ " .. tostring(sprite_texture))

		local sprite 	= {}
		-- sprite.sheettex = pxf.texture( spritepath, {minfilter = gl.GL_NEAREST, magfilter = gl.GL_NEAREST} )
		sprite.sheettex = sprite_texture
		sprite.statetex = pxf.texture(nil, {minfilter = gl.GL_NEAREST, magfilter = gl.GL_NEAREST, internalformat = gl.GL_RGBA32F, datatype = gl.GL_FLOAT})
		sprite.state 	= {}
		sprite.tiles 	= pxf.quadbatch()
		sprite.size 	= size
		sprite.numtiles = {sprite.sheettex.width / sprite.size[1],sprite.sheettex.height / sprite.size[2]}
		sprite.animdata = {}

		sprite.animation = function( self, name, freq, ... )
			sprite.animdata[name] = {}
			sprite.animdata[name].frames = {}
			sprite.animdata[name].freq = freq

			for k,v in pairs(...) do 
				table.insert(sprite.animdata[name].frames,v) 
			end

			print("new animation w/ " .. #sprite.animdata[name].frames.. " frames")

			-- a.frame  = -1
			-- a.time   = freq
			-- a.frames = frames
			-- a.sprite = { sprite, x, y }
			-- a.next 	 = function(self,dt)
			-- 	if self.time >= freq then
			-- 		self.frame = ( self.frame + 1 ) % #self.frames
			-- 		self.time  = 0

			-- 		local framex = self.frames[ self.frame+1][1]
			-- 		local framey = self.frames[ self.frame+1][2]

			-- 		self.sprite[1]:update( self.sprite[2], self.sprite[3], { 0,0,framex,framey } )
			-- 	end

			-- 	self.time = self.time + dt
			-- end

			-- return a
		end

		sprite.animate = function(self,sprite_idx,dt)

		end

		sprite.create = function(self,num)
			if not num then num = 1 end

			assert( num < tilesperrow )

			self.tiles:begin()

			for i=1,num,1 do
				self.tiles.depth = i
				self.tiles:addtopleft( 0, 0, 1, 1 )
			end

			self.tiles:finish()

			local width  = num
			local height = 1

			for y=1,height,1 do 
				self.state[y] = {}
				for x=1,width,1 do 
					self.state[y][x] = {0,0,0,0}
				end
			end

			self.statetex:uploadempty(width,height)

			return self
		end	

		sprite.commit = function(self)
			local datasize 	= self.statetex.width * self.statetex.height * 4
			local td 		= ffi.new( "float[?]", datasize )

			for y=0,self.statetex.height-1,1 do
				for x=0,self.statetex.width-1,1 do
					local offset = ( y * self.statetex.width + x  ) * 4

					td[offset + 0] = self.state[y+1][x+1][1]
					td[offset + 1] = self.state[y+1][x+1][2]
					td[offset + 2] = self.state[y+1][x+1][3]
					td[offset + 3] = self.state[y+1][x+1][4]
				end
			end

			self.statetex:upload(td)
			self.docommit = false

			return self
		end

	    sprite.update 	= function(self, x,y, data)
	    	self.state[y+1][x+1] = data
	    	self.docommit = true

	    	return self
		end

		sprite.draw 	= function(self, shader, bindbuffers)
			if self.docommit then self:commit() end

			self.statetex:bind(0)
			self.sheettex:bind(1)

			shader:setuniform("fv", "tex0dim", { self.statetex.width, self.statetex.height })
			shader:setuniform( "i", "tex0", 0 )
			shader:setuniform( "i", "tex1", 1 )
			shader:setuniform( "fv", "tiles", self.numtiles )

			self.tiles:draw( shader, bindbuffers )
			self.statetex:unbind()
			self.sheettex:unbind()

			return self
		end

		self.sprites[sprite_texture] = sprite

		return sprite
	end

	function s:draw()
		for k,v in pairs(self.sprites) do
			v:draw()
		end
	end

	spritemanager = s
	return spritemanager
end

return spritemanager_generator()
