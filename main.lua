package.path = package.path .. "./pxf/?.lua;./?.lua;"
package.cpath = package.cpath .. ";./ufo/bin/Linux/x64/?.so;"

pxfaux = require( "pxfaux" )

local game = require( "game/game" )

game:init({
      win_w = 1024,
      win_h = 720,
      fullscreen = false
   })
game:push_screen( require("game/boot") )
game:run()
