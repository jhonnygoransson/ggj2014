attribute vec3 position;
attribute vec2 uv0;
attribute vec3 normal;

uniform mat4 pmtx;
uniform mat4 mmtx;

varying vec2 v_uv0;
varying vec3 v_color;

void main()
{
	v_uv0   = vec2(uv0.s, 1.0 - uv0.t);
	v_color = normal;

	gl_Position = pmtx * mmtx * vec4(position.xyz, 1.0);
	
}
