CheckVersion("0.4")

local settings = NewSettings()
settings.debug = 0

if family == "unix" then
	if platform == "macosx" then
		settings.cc.includes:Add("/usr/include/luajit-2.0/")
		settings.dll.libs:Add("luajit-5.1.2.0.2")
		settings.dll.frameworks:Add("OpenGL")
		settings.dll.frameworks:Add("Carbon")
	elseif platform == "linux" then
		settings.cc.includes:Add("/usr/include/luajit-2.0/")
		settings.cc.flags:Add("-fPIC")
		settings.dll.libs:Add("GL")
		settings.dll.libs:Add("glfw")
	end
elseif family == "windows" then
	settings.cc.flags:Add("/MD")
	-- settings.cc.flags:Add("/MT")
	settings.cc.flags:Add("/Od")
	settings.cc.flags:Add("/Z7")
	settings.cc.flags:Add("/EHsc")

	-- Link, it's me zelda
	----------------------
	settings.dll.libs:Add("lua51_")
	settings.dll.libs:Add("opengl32")
end

settings.cc.includes:Add("soil/include")


-- compile sources
------------------

-- core
aux_coresrc = { "pxfaux.c", "tex.c", CollectRecursive("soil/src/*.c") }

-- lpack
aux_lpacksrc = { "lpack/lpack.c" }

-- tex

aux_objs = Compile( settings, aux_coresrc, aux_lpacksrc )

SharedLibrary(settings, "pxfaux", aux_objs )
