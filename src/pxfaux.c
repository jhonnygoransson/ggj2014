#define LUA_LIB

#include "pxfaux.h"

#include "lpack/lpack.h"

int LUA_MODULE luaopen_pxfaux(lua_State* L)
{
    static const luaL_reg R[] = {
        { "bpack",   l_pack },
        { "bunpack", l_unpack },
        { "tex_load", tex_load},
        { NULL, NULL }
    };

    luaL_register( L, "pxfaux", R );

    return 1;
}
